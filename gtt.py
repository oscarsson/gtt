#! /usr/bin/python
# -*- coding: utf-8 -*-

#    gtt, a tool to record your working time.
#    Copyright (C) 2009  Mårten Oscarsson
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.

import Xlib.display
import time
import os
import gtk
import gobject
import sys
import pango
import socket
import select

# XPM
gtt48_xpm = [
"48 48 725 2",
"  	c None",
". 	c #9F9F9F",
"+ 	c #ABABAB",
"@ 	c #C0C0C0",
"# 	c #D1D0D1",
"$ 	c #D7D7D7",
"% 	c #C4C4C4",
"& 	c #B2B1B2",
"* 	c #AFAFAF",
"= 	c #B7B7B7",
"- 	c #A6A6A6",
"; 	c #B7B6B7",
"> 	c #505050",
", 	c #6B6B6B",
"' 	c #808080",
") 	c #7F7F7F",
"! 	c #747474",
"~ 	c #676767",
"{ 	c #545454",
"] 	c #CAC9CA",
"^ 	c #757575",
"/ 	c #888988",
"( 	c #B4B3B4",
"_ 	c #DDDDDD",
": 	c #EAEAEA",
"< 	c #EFEEEF",
"[ 	c #F0EEF0",
"} 	c #EEEEEE",
"| 	c #F0EFF0",
"1 	c #ECECEC",
"2 	c #D5D5D5",
"3 	c #A6A5A6",
"4 	c #8E8E8E",
"5 	c #888888",
"6 	c #CACACA",
"7 	c #484848",
"8 	c #E4E3E4",
"9 	c #535353",
"0 	c #9B9B9B",
"a 	c #B4B5B4",
"b 	c #D7D6D7",
"c 	c #F1EFF1",
"d 	c #E5E4E5",
"e 	c #DEDDDE",
"f 	c #DBD9DB",
"g 	c #D6D5D6",
"h 	c #D9D7D9",
"i 	c #DBDADB",
"j 	c #DFDEDF",
"k 	c #EBEAEB",
"l 	c #F6F4F6",
"m 	c #D2D2D2",
"n 	c #B9B9B9",
"o 	c #8B8B8B",
"p 	c #808181",
"q 	c #BFC0C0",
"r 	c #BFBFBF",
"s 	c #D7D4D4",
"t 	c #ABA8A8",
"u 	c #828282",
"v 	c #C6C6C6",
"w 	c #F2F0F2",
"x 	c #EEEDEE",
"y 	c #C7C6C7",
"z 	c #C4C3C4",
"A 	c #C1C1C1",
"B 	c #BDBDBD",
"C 	c #B8B8B8",
"D 	c #B6B6B6",
"E 	c #B7B8B7",
"F 	c #BFBEBF",
"G 	c #C2C2C2",
"H 	c #C3C2C3",
"I 	c #CBCBCB",
"J 	c #DFDDDF",
"K 	c #F2F1F2",
"L 	c #DCDCDC",
"M 	c #BBBBBB",
"N 	c #575757",
"O 	c #D7D6D6",
"P 	c #D4D2D2",
"Q 	c #ADA9A9",
"R 	c #E1DDDD",
"S 	c #BDBABA",
"T 	c #282828",
"U 	c #B2B2B2",
"V 	c #606060",
"W 	c #979797",
"X 	c #D5D4D5",
"Y 	c #ECEBEC",
"Z 	c #BEBDBE",
"` 	c #ACADAC",
" .	c #989898",
"..	c #868585",
"+.	c #767676",
"@.	c #6A6A6A",
"#.	c #626262",
"$.	c #646363",
"%.	c #636161",
"&.	c #676666",
"*.	c #727272",
"=.	c #838383",
"-.	c #969797",
";.	c #E1E0E1",
">.	c #EAE9EA",
",.	c #D2D1D2",
"'.	c #7E7E7E",
").	c #B2B3B3",
"!.	c #E3E3E3",
"~.	c #C4C1C1",
"{.	c #D6D2D2",
"].	c #C9C5C5",
"^.	c #DCDADA",
"/.	c #CFCECF",
"(.	c #4F4F4F",
"_.	c #BCBBBC",
":.	c #B0AFB0",
"<.	c #E6E5E6",
"[.	c #F5F3F5",
"}.	c #C3C3C3",
"|.	c #9C9C9C",
"1.	c #777676",
"2.	c #777575",
"3.	c #7B7979",
"4.	c #7F7C7C",
"5.	c #8C8989",
"6.	c #8F8B8B",
"7.	c #908D8D",
"8.	c #8F8C8C",
"9.	c #8B8888",
"0.	c #848080",
"a.	c #6A6868",
"b.	c #676565",
"c.	c #626161",
"d.	c #737373",
"e.	c #A2A2A2",
"f.	c #E9E8E9",
"g.	c #F3F1F3",
"h.	c #8A8A8A",
"i.	c #878888",
"j.	c #D2CFCF",
"k.	c #D3CFCF",
"l.	c #DBDAD9",
"m.	c #C9C6C9",
"n.	c #7E7B7E",
"o.	c #878687",
"p.	c #5F5F5F",
"q.	c #767675",
"r.	c #726F6F",
"s.	c #9D9999",
"t.	c #A4A1A1",
"u.	c #ACA8A8",
"v.	c #787676",
"w.	c #B4B0B0",
"x.	c #B2AEAE",
"y.	c #B1ADAD",
"z.	c #AFABAB",
"A.	c #A7A4A4",
"B.	c #A09C9C",
"C.	c #757272",
"D.	c #918E8E",
"E.	c #878484",
"F.	c #706E6E",
"G.	c #4E4D4D",
"H.	c #B1B1B1",
"I.	c #E2E1E2",
"J.	c #F4F1F4",
"K.	c #EDEBED",
"L.	c #909090",
"M.	c #6F6E6E",
"N.	c #C0BEBE",
"O.	c #A7A6A7",
"P.	c #D0CFCF",
"Q.	c #CBC8CB",
"R.	c #7D7B7D",
"S.	c #7D7D7D",
"T.	c #5B5B5B",
"U.	c #ECEAEC",
"V.	c #929292",
"W.	c #747272",
"X.	c #9C9898",
"Y.	c #B8B4B4",
"Z.	c #BCB4B4",
"`.	c #C4C0C0",
" +	c #CECACA",
".+	c #8A8787",
"++	c #D4CFCF",
"@+	c #D0CDCD",
"#+	c #D0CCCC",
"$+	c #CBC7C7",
"%+	c #C6C1C1",
"&+	c #AEABAB",
"*+	c #9E9A9A",
"=+	c #AEAAAA",
"-+	c #A4A0A0",
";+	c #999696",
">+	c #8C8A8A",
",+	c #666464",
"'+	c #5D5D5D",
")+	c #D8D8D8",
"!+	c #F3F2F3",
"~+	c #6E6E6F",
"{+	c #767376",
"]+	c #6D6C6D",
"^+	c #555555",
"/+	c #B0B1B0",
"(+	c #E5E5E5",
"_+	c #E9E9E9",
":+	c #C8C8C8",
"<+	c #818080",
"[+	c #888686",
"}+	c #B9B5B5",
"|+	c #CAC5C5",
"1+	c #D9D1D1",
"2+	c #DBCACA",
"3+	c #E6E2E2",
"4+	c #C7C3C3",
"5+	c #E2DEDE",
"6+	c #E7E3E3",
"7+	c #E5E1E1",
"8+	c #E4E0E0",
"9+	c #CCC8C8",
"0+	c #C1BDBD",
"a+	c #B5B2B2",
"b+	c #A8A5A5",
"c+	c #9A9898",
"d+	c #7E7C7C",
"e+	c #515151",
"f+	c #F1F0F1",
"g+	c #E8E8E8",
"h+	c #959695",
"i+	c #DEDCDE",
"j+	c #E2E0E2",
"k+	c #C7C7C7",
"l+	c #7A7A7A",
"m+	c #949292",
"n+	c #DBD7D7",
"o+	c #E5E0E0",
"p+	c #E9E3E3",
"q+	c #E7E2E2",
"r+	c #D9B8B8",
"s+	c #E9E4E4",
"t+	c #DFDADA",
"u+	c #D2CECE",
"v+	c #A5A2A2",
"w+	c #939090",
"x+	c #585656",
"y+	c #858585",
"z+	c #E7E7E7",
"A+	c #E6E4E6",
"B+	c #7B7B7B",
"C+	c #CDCCCD",
"D+	c #DCDADC",
"E+	c #CDCDCD",
"F+	c #8D8A8A",
"G+	c #BEBABA",
"H+	c #ECE7E7",
"I+	c #E9E5E5",
"J+	c #DAC3C3",
"K+	c #E0C9C9",
"L+	c #E9E6E6",
"M+	c #E8E4E4",
"N+	c #D6D1D1",
"O+	c #B9B6B6",
"P+	c #858383",
"Q+	c #888585",
"R+	c #515050",
"S+	c #959595",
"T+	c #EDECED",
"U+	c #B8B7B8",
"V+	c #7C7C7C",
"W+	c #444444",
"X+	c #AEADAE",
"Y+	c #D3D2D3",
"Z+	c #8D8C8C",
"`+	c #E3DFDF",
" @	c #F4EFEF",
".@	c #EBE7E7",
"+@	c #E2D7D7",
"@@	c #D19E9E",
"#@	c #EAE6E6",
"$@	c #F5F1F1",
"%@	c #979494",
"&@	c #B8B5B5",
"*@	c #B6B2B2",
"=@	c #8F8D8D",
"-@	c #474646",
";@	c #AAAAAA",
">@	c #E1E1E1",
",@	c #A9A9A9",
"'@	c #A4A5A5",
")@	c #D7D7D8",
"!@	c #D0CFD0",
"~@	c #BEBEBE",
"{@	c #D9D5D5",
"]@	c #EDE9E9",
"^@	c #BDB9B9",
"/@	c #9F9C9C",
"(@	c #D8D3D3",
"_@	c #EBE6E6",
":@	c #EAE8E8",
"<@	c #C68282",
"[@	c #E6D9D9",
"}@	c #EBE8E8",
"|@	c #E0DCDC",
"1@	c #A6A3A3",
"2@	c #A7A5A5",
"3@	c #D4D1D1",
"4@	c #CAC7C7",
"5@	c #B7B4B4",
"6@	c #D6D6D6",
"7@	c #4E4E4E",
"8@	c #525152",
"9@	c #A8A7A8",
"0@	c #DADADB",
"a@	c #494949",
"b@	c #797979",
"c@	c #A19F9F",
"d@	c #EEEAEA",
"e@	c #D7D3D3",
"f@	c #ECE8E8",
"g@	c #DFD7D7",
"h@	c #BD5959",
"i@	c #EEF0F0",
"j@	c #A9A6A6",
"k@	c #A3A0A0",
"l@	c #C6C2C2",
"m@	c #C0BFBF",
"n@	c #595959",
"o@	c #4A4A4A",
"p@	c #383838",
"q@	c #707070",
"r@	c #ACACAC",
"s@	c #696868",
"t@	c #ECEEEE",
"u@	c #C07979",
"v@	c #D7A8A8",
"w@	c #EDECEC",
"x@	c #D3D0D0",
"y@	c #999797",
"z@	c #DEDBDB",
"A@	c #DAD9D9",
"B@	c #D4D4D4",
"C@	c #464646",
"D@	c #393939",
"E@	c #343434",
"F@	c #8F8F8F",
"G@	c #888788",
"H@	c #C9C8C9",
"I@	c #787878",
"J@	c #F0ECEC",
"K@	c #ECE9E9",
"L@	c #B84A4A",
"M@	c #C1BEBE",
"N@	c #D1D0D0",
"O@	c #D3D3D3",
"P@	c #4C4C4C",
"Q@	c #3A3A3A",
"R@	c #858485",
"S@	c #F5F4F5",
"T@	c #C1C0C1",
"U@	c #646464",
"V@	c #EEEBEB",
"W@	c #DED9D9",
"X@	c #DCD3D4",
"Y@	c #E1DEDE",
"Z@	c #DEDADA",
"`@	c #EAE7E7",
" #	c #BE7272",
".#	c #D9ADAD",
"+#	c #EDEAEA",
"@#	c #EFECEC",
"##	c #E3E0E0",
"$#	c #F1EDED",
"%#	c #E6E3E3",
"&#	c #D1D1D1",
"*#	c #3B3B3B",
"=#	c #565656",
"-#	c #F6F5F6",
";#	c #A0A0A0",
">#	c #848383",
",#	c #CFCCCC",
"'#	c #AFACAC",
")#	c #E8E5E5",
"!#	c #DDDADB",
"~#	c #E5CECE",
"{#	c #F0EFEF",
"]#	c #D8D5D5",
"^#	c #D3AFAF",
"/#	c #BB5757",
"(#	c #EFEEEE",
"_#	c #EFEBEB",
":#	c #8E8D8D",
"<#	c #F2EFEF",
"[#	c #DDDCDC",
"}#	c #3C3C3C",
"|#	c #414141",
"1#	c #DFDFDF",
"2#	c #B8B6B8",
"3#	c #A8A6A6",
"4#	c #CFCDCD",
"5#	c #E7E4E4",
"6#	c #E8E6E6",
"7#	c #C3AAAA",
"8#	c #E4D5D5",
"9#	c #AB2D2D",
"0#	c #E0BEBE",
"a#	c #F0EEEE",
"b#	c #F0EBEB",
"c#	c #717171",
"d#	c #EFEAEA",
"e#	c #3F3F3F",
"f#	c #494848",
"g#	c #C8C7C8",
"h#	c #E7E5E7",
"i#	c #818181",
"j#	c #666666",
"k#	c #B6B5B6",
"l#	c #828181",
"m#	c #B0AEAE",
"n#	c #DCCACA",
"o#	c #CFBDBD",
"p#	c #EEECEC",
"q#	c #EFEFEF",
"r#	c #C79090",
"s#	c #AD2626",
"t#	c #B5B5B5",
"u#	c #656565",
"v#	c #CAC8C8",
"w#	c #F3F0F0",
"x#	c #D0D0D0",
"y#	c #5F605F",
"z#	c #FBF8F8",
"A#	c #F3EFEF",
"B#	c #595858",
"C#	c #1B1B1B",
"D#	c #717071",
"E#	c #A8A8A8",
"F#	c #BAB8B8",
"G#	c #E1DDDE",
"H#	c #E5E2E2",
"I#	c #F5F2F2",
"J#	c #717676",
"K#	c #990000",
"L#	c #770000",
"M#	c #412626",
"N#	c #CCC9C9",
"O#	c #E8E7E7",
"P#	c #C9C7C9",
"Q#	c #555556",
"R#	c #616161",
"S#	c #9F9E9E",
"T#	c #F2F0F0",
"U#	c #EFEDED",
"V#	c #F8F6F6",
"W#	c #6C6B6B",
"X#	c #A1A1A1",
"Y#	c #1E1E1E",
"Z#	c #191919",
"`#	c #2B2B2B",
" $	c #727172",
".$	c #ADADAD",
"+$	c #A4A4A4",
"@$	c #C4C2C2",
"#$	c #F0EDED",
"$$	c #EBE7E8",
"%$	c #ECE8E9",
"&$	c #EDEBEB",
"*$	c #E6E6E6",
"=$	c #4F3F3F",
"-$	c #A00000",
";$	c #A30000",
">$	c #4B0000",
",$	c #F6F3F3",
"'$	c #F1EEEE",
")$	c #E3E2E3",
"!$	c #CECACC",
"~$	c #8A8384",
"{$	c #363636",
"]$	c #A6A5A5",
"^$	c #F9F5F5",
"/$	c #EDEDED",
"($	c #F4F2F4",
"_$	c #939392",
":$	c #575656",
"<$	c #303030",
"[$	c #0C0C0C",
"}$	c #070707",
"|$	c #6C6C6C",
"1$	c #A9A8A9",
"2$	c #BBB9B9",
"3$	c #EDEAEB",
"4$	c #5D5F5F",
"5$	c #770808",
"6$	c #950000",
"7$	c #3C0505",
"8$	c #D1CFCF",
"9$	c #ECEAE9",
"0$	c #E0DFE0",
"a$	c #CDC9CB",
"b$	c #9F9798",
"c$	c #9D989C",
"d$	c #BEBCBD",
"e$	c #F7F4F4",
"f$	c #F1EFEF",
"g$	c #FAF7F7",
"h$	c #7A7979",
"i$	c #6D6E6E",
"j$	c #191A1A",
"k$	c #000000",
"l$	c #010101",
"m$	c #A4A3A4",
"n$	c #9D9D9D",
"o$	c #929191",
"p$	c #878383",
"q$	c #763333",
"r$	c #A43636",
"s$	c #F4F3F3",
"t$	c #D0CDCE",
"u$	c #CCC8C9",
"v$	c #9C9494",
"w$	c #A09DA1",
"x$	c #DCDBDC",
"y$	c #FCFAFA",
"z$	c #A5A5A5",
"A$	c #E5E3E5",
"B$	c #5C5D5C",
"C$	c #9E9D9E",
"D$	c #9C9B9C",
"E$	c #F4F2F2",
"F$	c #D3D1D1",
"G$	c #6D6D6D",
"H$	c #E4E2E2",
"I$	c #E4D3D3",
"J$	c #A51717",
"K$	c #E7CECE",
"L$	c #9F9CA1",
"M$	c #F4F1F1",
"N$	c #FFFDFD",
"O$	c #F4F3F4",
"P$	c #5C5C5C",
"Q$	c #949294",
"R$	c #7F7E7E",
"S$	c #B9B7B7",
"T$	c #E7E5E5",
"U$	c #F3F1F1",
"V$	c #F5F3F3",
"W$	c #F2F4F4",
"X$	c #C27474",
"Y$	c #CE8888",
"Z$	c #E0E3E3",
"`$	c #9E9697",
" %	c #A19DA1",
".%	c #DDDBDC",
"+%	c #F6F4F4",
"@%	c #DFDEDE",
"#%	c #B7B5B5",
"$%	c #C9C8C8",
"%%	c #E1E0E0",
"&%	c #CFCFCF",
"*%	c #838283",
"=%	c #969696",
"-%	c #F2EEEE",
";%	c #F3EDED",
">%	c #E4E4E4",
",%	c #E0DEDF",
"'%	c #9A9292",
")%	c #DEDCDD",
"!%	c #9C9B9B",
"~%	c #686868",
"{%	c #868686",
"]%	c #646564",
"^%	c #959395",
"/%	c #919191",
"(%	c #C2C1C1",
"_%	c #F7F5F5",
":%	c #C9C7C7",
"<%	c #DBD8D9",
"[%	c #C7C2C2",
"}%	c #C5C3C7",
"|%	c #FAF8F8",
"1%	c #6D6C6C",
"2%	c #CBC9CB",
"3%	c #6F6F6F",
"4%	c #434343",
"5%	c #8F8E8F",
"6%	c #929192",
"7%	c #636262",
"8%	c #D2D0D1",
"9%	c #A49D9E",
"0%	c #B8B5B8",
"a%	c #CCCCCC",
"b%	c #E8E7E8",
"c%	c #939293",
"d%	c #8C8C8C",
"e%	c #4B4B4B",
"f%	c #AFAEAE",
"g%	c #F9F7F7",
"h%	c #E6E5E5",
"i%	c #E3E1E1",
"j%	c #C2C0C0",
"k%	c #CBC8C9",
"l%	c #9C9394",
"m%	c #B0ACB0",
"n%	c #E5E4E4",
"o%	c #F5F4F4",
"p%	c #F6F5F5",
"q%	c #9A9999",
"r%	c #B4B3B3",
"s%	c #F6F5F4",
"t%	c #E9E7E8",
"u%	c #E2E0E0",
"v%	c #A9A7A7",
"w%	c #9D9595",
"x%	c #AAA7AB",
"y%	c #F3F2F2",
"z%	c #FBF9F9",
"A%	c #BEBDBD",
"B%	c #A8A7A7",
"C%	c #ECEAEA",
"D%	c #484747",
"E%	c #B4B4B4",
"F%	c #B1AFB1",
"G%	c #8D8C8D",
"H%	c #8A898A",
"I%	c #999999",
"J%	c #C5C4C4",
"K%	c #BDBCBC",
"L%	c #9B9898",
"M%	c #A39FA2",
"N%	c #F7F6F6",
"O%	c #D6D4D4",
"P%	c #F1F0F0",
"Q%	c #F9F8F8",
"R%	c #6E6E6E",
"S%	c #C8C6C8",
"T%	c #3E3E3E",
"U%	c #7E7D7E",
"V%	c #BBBABA",
"W%	c #D8D3D4",
"X%	c #EAE9E9",
"Y%	c #E9E8E8",
"Z%	c #EBEAEA",
"`%	c #DEDDDD",
" &	c #A7A6A6",
".&	c #807F7F",
"+&	c #C2C0C2",
"@&	c #2A2A2A",
"#&	c #868586",
"$&	c #8C8B8C",
"%&	c #7A7B7A",
"&&	c #BCBCBC",
"*&	c #F8F7F7",
"=&	c #A7A7A7",
"-&	c #ABA9AA",
";&	c #B6B7B7",
">&	c #C7C6C6",
",&	c #D3D2D2",
"'&	c #DADADA",
")&	c #C6C5C6",
"!&	c #323232",
"~&	c #605F60",
"{&	c #949494",
"]&	c #B1B0B0",
"^&	c #E3E2E2",
"/&	c #FFFFFF",
"(&	c #4D4D4D",
"_&	c #D7D5D7",
":&	c #A2A1A2",
"<&	c #848384",
"[&	c #898989",
"}&	c #FFFEFE",
"|&	c #FDFCFC",
"1&	c #FAF9F9",
"2&	c #878787",
"3&	c #BBBABB",
"4&	c #AAA9AA",
"5&	c #7D7C7D",
"6&	c #585757",
"7&	c #B7B6B6",
"8&	c #A9A8A8",
"9&	c #FAFAFA",
"0&	c #A3A2A2",
"a&	c #848484",
"b&	c #C9C9C9",
"c&	c #DAD9DA",
"d&	c #C0BFC0",
"e&	c #ACABAC",
"f&	c #747374",
"g&	c #8F9090",
"h&	c #B3B3B3",
"i&	c #FCFBFB",
"j&	c #FEFDFD",
"k&	c #797878",
"l&	c #858686",
"m&	c #B3B2B3",
"n&	c #9A999A",
"o&	c #333333",
"p&	c #6C6B6C",
"q&	c #9B9A9A",
"r&	c #949393",
"s&	c #8D8D8D",
"t&	c #B1B0B1",
"u&	c #454545",
"v&	c #6E6D6E",
"w&	c #777777",
"x&	c #B0B0B0",
"y&	c #9E9E9E",
"z&	c #A09FA0",
"A&	c #4D4C4D",
"B&	c #616061",
"C&	c #6B6A6B",
"D&	c #959495",
"E&	c #AEAEAE",
"F&	c #585858",
"G&	c #424242",
"H&	c #706F70",
"I&	c #767576",
"J&	c #818081",
"K&	c #5F5E5F",
"L&	c #2F2F2F",
"M&	c #3D3D3D",
"N&	c #474747",
"O&	c #5A5A5A",
"P&	c #525252",
"                                                                                  .             ",
"                                                                            + @ # $ % &         ",
"                                                                          * =           - ;     ",
"                                      > , ' ) ! ~ {                       .               ]     ",
"                              ^ / ( _ : < [ } < | 1 2 3 4                 5 % 6           7 8   ",
"                        9 0 a b | c d e f g g h i j k l < m n o         p q r s t           ]   ",
"                      u v i w x g y z A B C D E F G H I J K [ L M N       O P Q R S       T U   ",
"                  V W X < Y b Z `  ...+.@.#.$.%.&.*.=.-.* % ;.| >.,.'.  ).!.~.{.].^./.    (._.  ",
"                @.:.<.[.f }.|.1.2.3.4.5.6.7.8.9.0.3.a.b.c.d.e.6 f.g.e h.i.^.j.k.l.8 m.n.> o.    ",
"              p.B | | # & q.r.s.t.u.v.w.x.y.z.u.A.B.C.D.E.F.G.^ H.I.J.K.L.M.N.O.P.Q.R.V S.      ",
"            T.n U.x 6 V.W.X.Y.Z.`. +.+++@+#+ +$+%+&+*+=+-+;+>+,+'+W )+!+< h.~+{+  ]+            ",
"          ^+/+(+_+:+<+[+}+|+k.1+2+3+4+5+6+3+7+8+5+k.9+9+0+a+b+c+d+e+u e f+g+u                   ",
"          h+i+j+k+l+m+ +n+o+p+q+r+s+s+s+s+s+s+s+s+s+s+q+t+u+~.a+v+w+x+y+z+K A+B+                ",
"        , C+D+E+'.F+ +G+H+I+I+I+J+K+I+I+I+I+I+I+I+I+I+I+L+M+N+k.O+P+Q+R+S+T+>.U+V+              ",
"      W+X+g Y+W Z+`+I+$+&+ @.@I++@@@#@I+I+I+I+I+I+I+I+I+#@$@%@$+t &@*@=@-@;@>@,@'@)@            ",
"      ' C+!@~@d.{@#@#@]@^@/@(@_@:@<@[@}@.@.@.@.@.@.@.@.@|@1@2@n+3@|@4@5@8.-.6@7@8@9@0@          ",
"    a@+ # ] b@c@d@.@.@.@5+t e@.@f@g@h@i@.@.@.@.@.@.@.@.@f@j@9+9+k@f@`+l@m@$ n@o@p@a@            ",
"    q@z y r@s@I+f@.@.@.@f@.@I+H+f@t@u@v@w@.@.@.@.@.@}@}@]@x@&@y@z@}@7+A@B@n@C@D@E@F@q@          ",
"    G@H@r I@w.J@f@K@K@K@K@#@#@K@]@]@|@L@H+]@]@]@]@]@]@]@M@e@]@]@]@]@N@O@P@o@Q@D@R@S@0           ",
"  a@e.T@U U@7+J@V@]@]@R W@X@Y@Z@`@]@.@ #.#+#+#+#+#@###t ~.$#+#d@%#N@&#n@o@*#E@=#-#-#;           ",
"  9 & U+;#>#,#'#].K@)#!#+#~#{#]#I+]@d@^#/#(#+#+#_#z@:#j.<#+#+#6+[#O@n@C@}#|#1.N 1#[.X ~         ",
"  '+2#& 4 3#V@V@4#%#5#6#K@7#8#+#}@K@_#I+9#0#a#b#Y@c#3@_#d#_#@#N@O@P@o@e#7@>+}@f#g#K h#i#        ",
"  j#k#+ l#m#_#_#$#_#I+`@]@n#o###+#+#p#q#r#s#U t#u#v#w#@#@#`@x#B@y#{ e#(.=@z#A#B#U f+T+L.        ",
"  D#& E#S.F#@#@#@#@#]@G#f@_#J@H#K@_#@#I#J#K#L#M#N#$@@#@#V@O#e P#Q#R#U@S#T#U#V#W#X#< f+- '+      ",
"   $.$+$B+@$#$#$#$#$#$$$%$K@K@@#&$a#a#*$=$-$;$>$2@,$'$'$%#@$)$!$~${$]$<#'$'$^$P+ ./$($_$:$      ",
"  |$1$X#+.2$'$'$'$'$'$'$'$+#3$'$'$'$'$+#4$5$6$7$8$w#<#9$P 0$a$b$c$d$e$f$f$f$g$h$0 /$g.i$        ",
"  V m$n$! m#'$'$'$'$'$'$'$f$f$'$'$'$w#o$'.p$q$r$s$<#9$t$j u$v$w$x$I#f$f$f$f$y$R#z$f+A$B$        ",
"  8@C$D$+.2@<#<#T#T#T#T#T#T#T#T#T#E$F$G$H$T#I$J$K$U#t$8 a$v$L$!#M$w#w#w#w#w#N$7@C O$y P$        ",
"  C@Q$ .i#R$)#F$S$T$U$w#w#w#w#w#w#w#<#V$E$U$W$X$Y$Z$j a$`$ %.%+%U$U$E$@%#%$%%%W+&%O$m$=#        ",
"  p@*%=%o =#[#z@-%M$w#w#w#U$U$U$U$U$U$E$U$U$V$;%>%1 ,%'% %)%I#U$U$U$U$E$+%:@!%~%8 8 {%          ",
"    ]%^%/%e+(%_%E$E$E$E$E$E$E$E$E$E$E$E$E$U$@%:%8 <%[%}%f +%V$V$V$V$V$V$V$|%1%W f+2%3%          ",
"    4%5%6%d.7%+%V$V$V$V$V$V$V$V$V$V$V$V$f$8%H$I.a$9%0%U#_%V$_%V$V$V$V$V$V$^.#.a%b%E##.          ",
"    `# $c%d%e%f%g%V$V$V$V$T$h%V$V$V$V$+%i%j%8 k%l%m%n%+%V$g%v#a#_%V$o%o%p%q%B+*$] =.            ",
"      o@h.4 ^ c.K@+%+%+%+%r%m@+%p%p%p%s%t%u%v%w%x%a#y%p%_%z%A%B%C%_%_%_%U$D%E%z F%@.            ",
"      `#@.G%H%N I%_%V#T#J%K%6#_%_%_%_%y%m D L%M%#$U$T#N%N%N%y$O%$%P%Q%p%R%I@A S%L.              ",
"        T%U%h.S.W+H._%V%u%y$V#_%_%_%_%n%+$B W%5+X%n%Y%:@X%X%Z%(#`% &X%.&R#O@f.+&S.              ",
"        @&9 #&$&%&D%&&(#*&*&*&*&*&*&*&=&;@- B%-&;@E%;&V%A%(%>&N#4#,&h.7@t#'&)&F@                ",
"          !&~&y+{&S.9 ]&V#|%g%g%g%Q%p#^&/&N$|%s$P%X%X%(#(#{#{#y%X%n$(&= g+_&:&~                 ",
"            {$U@<&I%y+T.[&a#}&Q%Q%|&J%Z%1&Q%Q%1&1&6#O 1&1&1&/&O#2&#.3&(+C+4&^                   ",
"              D@R#5&|.=%*.6&7&Q%1&/&8&/&1&1&1&1&1&9&B 9&9&p%0&@.a&b&c&d&e&!                     ",
"                *#P$f&V.X#g&#.I@h&B@= |&i&}&}&j&i&1&K%P.r%k&l&z$6 H@m&n&q@                      ",
"                  o&e+p&5&S+,@|.i#b@i#5 o$q&+$0 r&s&4 F@/%r@&&M t&3 o.j#                        ",
"                    @&u&#.v&w&{&x&h&+ X#0  . .y&=&H.E%H.n e&y&z&{&R%                            ",
"                        {$A&B&C&D#<&D&+$* H.H.E&4&. D$W 6%6%2&! F&                              ",
"                            {$G&{ U@H&I&b@U%J&<&R@o.y+B+G$K&>                                   ",
"                                  L&M&N&> F&O&T.n@P&N&T%                                        ",
"                                                                                                ",
"                                                                                                "]




gtt48NoReport_xpm = [
"48 48 751 2",
"  	c None",
". 	c #9F9F9F",
"+ 	c #ABABAB",
"@ 	c #C0C0C0",
"# 	c #D1D0D1",
"$ 	c #D7D7D7",
"% 	c #C4C4C4",
"& 	c #B2B1B2",
"* 	c #AFAFAF",
"= 	c #B7B7B7",
"- 	c #A6A6A6",
"; 	c #B7B6B7",
"> 	c #505050",
", 	c #6B6B6B",
"' 	c #808080",
") 	c #7F7F7F",
"! 	c #747474",
"~ 	c #676767",
"{ 	c #545454",
"] 	c #CAC9CA",
"^ 	c #757575",
"/ 	c #888988",
"( 	c #B4B3B4",
"_ 	c #DDDDDD",
": 	c #EAEAEA",
"< 	c #EFEEEF",
"[ 	c #F0EEF0",
"} 	c #EEEEEE",
"| 	c #F0EFF0",
"1 	c #ECECEC",
"2 	c #D5D5D5",
"3 	c #A6A5A6",
"4 	c #8E8E8E",
"5 	c #888888",
"6 	c #CACACA",
"7 	c #484848",
"8 	c #E4E3E4",
"9 	c #535353",
"0 	c #9B9B9B",
"a 	c #B4B5B4",
"b 	c #D7D6D7",
"c 	c #F1EFF1",
"d 	c #E5E4E5",
"e 	c #DEDDDE",
"f 	c #DBD9DB",
"g 	c #D6D5D6",
"h 	c #D9D7D9",
"i 	c #DBDADB",
"j 	c #DFDEDF",
"k 	c #EBEAEB",
"l 	c #F6F4F6",
"m 	c #D2D2D2",
"n 	c #B9B9B9",
"o 	c #8B8B8B",
"p 	c #808181",
"q 	c #BFC0C0",
"r 	c #BFBFBF",
"s 	c #D7D4D4",
"t 	c #ABA8A8",
"u 	c #828282",
"v 	c #C6C6C6",
"w 	c #F2F0F2",
"x 	c #EEEDEE",
"y 	c #C7C6C7",
"z 	c #C4C3C4",
"A 	c #C1C1C1",
"B 	c #BDBDBD",
"C 	c #B8B8B8",
"D 	c #B6B6B6",
"E 	c #B7B8B7",
"F 	c #BFBEBF",
"G 	c #C2C2C2",
"H 	c #C3C2C3",
"I 	c #CBCBCB",
"J 	c #DFDDDF",
"K 	c #F2F1F2",
"L 	c #DCDCDC",
"M 	c #BBBBBB",
"N 	c #575757",
"O 	c #D7D6D6",
"P 	c #D4D2D2",
"Q 	c #ADA9A9",
"R 	c #E1DDDD",
"S 	c #BDBABA",
"T 	c #282828",
"U 	c #B2B2B2",
"V 	c #606060",
"W 	c #979797",
"X 	c #D5D4D5",
"Y 	c #ECEBEC",
"Z 	c #BEBDBE",
"` 	c #ACADAC",
" .	c #989898",
"..	c #868585",
"+.	c #767676",
"@.	c #6A6A6A",
"#.	c #626262",
"$.	c #646363",
"%.	c #636161",
"&.	c #676666",
"*.	c #727272",
"=.	c #838383",
"-.	c #969797",
";.	c #E1E0E1",
">.	c #EAE9EA",
",.	c #D2D1D2",
"'.	c #7E7E7E",
").	c #B2B3B3",
"!.	c #E3E3E3",
"~.	c #C4C1C1",
"{.	c #D6D2D2",
"].	c #C9C5C5",
"^.	c #DCDADA",
"/.	c #CFCECF",
"(.	c #4F4F4F",
"_.	c #BCBBBC",
":.	c #B0AFB0",
"<.	c #E6E5E6",
"[.	c #F5F3F5",
"}.	c #C3C3C3",
"|.	c #9C9C9C",
"1.	c #777676",
"2.	c #777575",
"3.	c #7B7979",
"4.	c #7F7C7C",
"5.	c #8C8989",
"6.	c #8F8B8B",
"7.	c #908D8D",
"8.	c #8F8C8C",
"9.	c #8B8888",
"0.	c #848080",
"a.	c #6A6868",
"b.	c #676565",
"c.	c #626161",
"d.	c #737373",
"e.	c #A2A2A2",
"f.	c #E9E8E9",
"g.	c #F3F1F3",
"h.	c #8A8A8A",
"i.	c #878888",
"j.	c #D2CFCF",
"k.	c #D3CFCF",
"l.	c #DBDAD9",
"m.	c #C9C6C9",
"n.	c #7E7B7E",
"o.	c #878687",
"p.	c #5F5F5F",
"q.	c #767675",
"r.	c #726F6F",
"s.	c #9D9999",
"t.	c #A4A1A1",
"u.	c #ACA8A8",
"v.	c #787676",
"w.	c #B4B0B0",
"x.	c #B2AEAE",
"y.	c #B1ADAD",
"z.	c #AFABAB",
"A.	c #A7A4A4",
"B.	c #A09C9C",
"C.	c #757272",
"D.	c #918E8E",
"E.	c #878484",
"F.	c #706E6E",
"G.	c #4E4D4D",
"H.	c #B1B1B1",
"I.	c #E2E1E2",
"J.	c #F4F1F4",
"K.	c #EDEBED",
"L.	c #909090",
"M.	c #6F6E6E",
"N.	c #C0BEBE",
"O.	c #A7A6A7",
"P.	c #D0CFCF",
"Q.	c #CBC8CB",
"R.	c #7D7B7D",
"S.	c #7D7D7D",
"T.	c #5B5B5B",
"U.	c #ECEAEC",
"V.	c #929292",
"W.	c #747272",
"X.	c #9C9898",
"Y.	c #B8B4B4",
"Z.	c #BCB4B4",
"`.	c #C4C0C0",
" +	c #CECACA",
".+	c #8A8787",
"++	c #D4CFCF",
"@+	c #D0CDCD",
"#+	c #D0CCCC",
"$+	c #CBC7C7",
"%+	c #C6C1C1",
"&+	c #AEABAB",
"*+	c #9E9A9A",
"=+	c #AEAAAA",
"-+	c #A4A0A0",
";+	c #999696",
">+	c #8C8A8A",
",+	c #666464",
"'+	c #5D5D5D",
")+	c #D8D8D8",
"!+	c #F3F2F3",
"~+	c #6E6E6F",
"{+	c #767376",
"]+	c #6D6C6D",
"^+	c #555555",
"/+	c #B0B1B0",
"(+	c #E5E5E5",
"_+	c #E9E9E9",
":+	c #C8C8C8",
"<+	c #818080",
"[+	c #888686",
"}+	c #B9B5B5",
"|+	c #CAC5C5",
"1+	c #D9D1D1",
"2+	c #DBCACA",
"3+	c #E6E2E2",
"4+	c #C7C3C3",
"5+	c #E2DEDE",
"6+	c #E7E3E3",
"7+	c #E5E1E1",
"8+	c #E4E0E0",
"9+	c #CCC8C8",
"0+	c #C1BDBD",
"a+	c #B5B2B2",
"b+	c #A8A5A5",
"c+	c #9A9898",
"d+	c #7E7C7C",
"e+	c #515151",
"f+	c #F1F0F1",
"g+	c #E8E8E8",
"h+	c #959695",
"i+	c #DEDCDE",
"j+	c #E2E0E2",
"k+	c #C7C7C7",
"l+	c #7A7A7A",
"m+	c #B86170",
"n+	c #F22248",
"o+	c #F70B37",
"p+	c #F7133D",
"q+	c #F1667F",
"r+	c #E7CDD1",
"s+	c #D9B8B8",
"t+	c #E9E4E4",
"u+	c #E7E2E2",
"v+	c #DFDADA",
"w+	c #D2CECE",
"x+	c #A5A2A2",
"y+	c #939090",
"z+	c #585656",
"A+	c #858585",
"B+	c #E7E7E7",
"C+	c #E6E4E6",
"D+	c #7B7B7B",
"E+	c #CDCCCD",
"F+	c #DCDADC",
"G+	c #CDCDCD",
"H+	c #8D8A8A",
"I+	c #F1284D",
"J+	c #F80B37",
"K+	c #F70C38",
"L+	c #F07289",
"M+	c #DBBABC",
"N+	c #E0C9C9",
"O+	c #E9E5E5",
"P+	c #E9E6E6",
"Q+	c #E8D9DB",
"R+	c #E18B9B",
"S+	c #F03255",
"T+	c #F4133D",
"U+	c #CF3451",
"V+	c #9E6C75",
"W+	c #515050",
"X+	c #959595",
"Y+	c #EDECED",
"Z+	c #B8B7B8",
"`+	c #7C7C7C",
" @	c #444444",
".@	c #AEADAE",
"+@	c #D3D2D3",
"@@	c #8D8C8C",
"#@	c #E3DFDF",
"$@	c #F7113B",
"%@	c #EB7C90",
"&@	c #D29799",
"*@	c #EAE6E6",
"=@	c #EADBDD",
"-@	c #F68B9E",
";@	c #F3103B",
">@	c #F5113B",
",@	c #DD4D67",
"'@	c #8F8D8D",
")@	c #474646",
"!@	c #AAAAAA",
"~@	c #E1E1E1",
"{@	c #A9A9A9",
"]@	c #A4A5A5",
"^@	c #D7D7D8",
"/@	c #D0CFD0",
"(@	c #BEBEBE",
"_@	c #D9D5D5",
":@	c #F52D52",
"<@	c #F7123D",
"[@	c #DC4D60",
"}@	c #E6CFD1",
"|@	c #EBE8E8",
"1@	c #EBE7E7",
"2@	c #EBDCDE",
"3@	c #EA7F92",
"4@	c #F5103A",
"5@	c #EE2F53",
"6@	c #B7B4B4",
"7@	c #D6D6D6",
"8@	c #4E4E4E",
"9@	c #525152",
"0@	c #A8A7A8",
"a@	c #DADADB",
"b@	c #494949",
"c@	c #797979",
"d@	c #A19F9F",
"e@	c #EEEAEA",
"f@	c #EF97A7",
"g@	c #F7153F",
"h@	c #F7123C",
"i@	c #D73649",
"j@	c #EEE5E7",
"k@	c #F08599",
"l@	c #F70D39",
"m@	c #EF5E79",
"n@	c #C6C2C2",
"o@	c #C0BFBF",
"p@	c #595959",
"q@	c #4A4A4A",
"r@	c #383838",
"s@	c #707070",
"t@	c #ACACAC",
"u@	c #696868",
"v@	c #ECE8E8",
"w@	c #EDBBC3",
"x@	c #F6294F",
"y@	c #F60E39",
"z@	c #E56275",
"A@	c #EDE1E3",
"B@	c #F08699",
"C@	c #E8B6BF",
"D@	c #DAD9D9",
"E@	c #D4D4D4",
"F@	c #464646",
"G@	c #393939",
"H@	c #343434",
"I@	c #8F8F8F",
"J@	c #888788",
"K@	c #C9C8C9",
"L@	c #787878",
"M@	c #F0ECEC",
"N@	c #ECE9E9",
"O@	c #EEBCC5",
"P@	c #F50D37",
"Q@	c #F18599",
"R@	c #EDDEE0",
"S@	c #EDE9E9",
"T@	c #F1869A",
"U@	c #EFBCC5",
"V@	c #D1D0D0",
"W@	c #D3D3D3",
"X@	c #4C4C4C",
"Y@	c #3A3A3A",
"Z@	c #858485",
"`@	c #F5F4F5",
" #	c #C1C0C1",
".#	c #646464",
"+#	c #EEEBEB",
"@#	c #DED9D9",
"##	c #E1ABB4",
"$#	c #F4274D",
"%#	c #F6103B",
"&#	c #F1879A",
"*#	c #EDDFE1",
"=#	c #E9B7C0",
"-#	c #D1D1D1",
";#	c #3B3B3B",
">#	c #565656",
",#	c #F6F5F6",
"'#	c #A0A0A0",
")#	c #848383",
"!#	c #CFCCCC",
"~#	c #AFACAC",
"{#	c #E8E5E5",
"]#	c #DDDADB",
"^#	c #EDEAEA",
"/#	c #E5CECE",
"(#	c #F1C1CA",
"_#	c #F3264C",
":#	c #F18195",
"<#	c #EAB7C0",
"[#	c #DDDCDC",
"}#	c #3C3C3C",
"|#	c #414141",
"1#	c #DFDFDF",
"2#	c #B8B6B8",
"3#	c #A8A6A6",
"4#	c #CFCDCD",
"5#	c #E6E3E3",
"6#	c #E7E4E4",
"7#	c #E8E6E6",
"8#	c #C3AAAA",
"9#	c #E4D5D5",
"0#	c #EFBDC6",
"a#	c #F70F3A",
"b#	c #F0BFC7",
"c#	c #3F3F3F",
"d#	c #494848",
"e#	c #C8C7C8",
"f#	c #E7E5E7",
"g#	c #818181",
"h#	c #666666",
"i#	c #B6B5B6",
"j#	c #828181",
"k#	c #B0AEAE",
"l#	c #EFEBEB",
"m#	c #F1EDED",
"n#	c #EAE7E7",
"o#	c #DCCACA",
"p#	c #CFBDBD",
"q#	c #E3E0E0",
"r#	c #ECBBC3",
"s#	c #D0D0D0",
"t#	c #5F605F",
"u#	c #FBF8F8",
"v#	c #F3EFEF",
"w#	c #595858",
"x#	c #1B1B1B",
"y#	c #717071",
"z#	c #A8A8A8",
"A#	c #BAB8B8",
"B#	c #EFECEC",
"C#	c #E1DDDE",
"D#	c #E5E2E2",
"E#	c #F0BEC7",
"F#	c #E8E7E7",
"G#	c #C9C7C9",
"H#	c #555556",
"I#	c #616161",
"J#	c #9F9E9E",
"K#	c #F2F0F0",
"L#	c #EFEDED",
"M#	c #F8F6F6",
"N#	c #6C6B6B",
"O#	c #A1A1A1",
"P#	c #1E1E1E",
"Q#	c #191919",
"R#	c #2B2B2B",
"S#	c #727172",
"T#	c #ADADAD",
"U#	c #A4A4A4",
"V#	c #C4C2C2",
"W#	c #F0EDED",
"X#	c #EBE7E8",
"Y#	c #ECE8E9",
"Z#	c #EDEBEB",
"`#	c #F0EEEE",
" $	c #F1B7C2",
".$	c #F61B44",
"+$	c #F71C44",
"@$	c #F2B7C2",
"#$	c #E3E2E3",
"$$	c #CECACC",
"%$	c #8A8384",
"&$	c #363636",
"*$	c #A6A5A5",
"=$	c #F2EFEF",
"-$	c #F1EEEE",
";$	c #F9F5F5",
">$	c #858383",
",$	c #EDEDED",
"'$	c #F4F2F4",
")$	c #939392",
"!$	c #575656",
"~$	c #303030",
"{$	c #0C0C0C",
"]$	c #070707",
"^$	c #6C6C6C",
"/$	c #A9A8A9",
"($	c #BBB9B9",
"_$	c #EDEAEB",
":$	c #F1E3E5",
"<$	c #F4899C",
"[$	c #F7113C",
"}$	c #D5C8CA",
"|$	c #E0DFE0",
"1$	c #CDC9CB",
"2$	c #9F9798",
"3$	c #9D989C",
"4$	c #BEBCBD",
"5$	c #F7F4F4",
"6$	c #F1EFEF",
"7$	c #FAF7F7",
"8$	c #7A7979",
"9$	c #6D6E6E",
"0$	c #191A1A",
"a$	c #000000",
"b$	c #010101",
"c$	c #A4A3A4",
"d$	c #9D9D9D",
"e$	c #F6113C",
"f$	c #EA8094",
"g$	c #CEBFC2",
"h$	c #9C9494",
"i$	c #A09DA1",
"j$	c #DCDBDC",
"k$	c #F5F2F2",
"l$	c #FCFAFA",
"m$	c #A5A5A5",
"n$	c #E5E3E5",
"o$	c #5C5D5C",
"p$	c #9E9D9E",
"q$	c #9C9B9C",
"r$	c #A7A5A5",
"s$	c #F2E5E7",
"t$	c #F48A9E",
"u$	c #C4576A",
"v$	c #A3959C",
"w$	c #F4F1F1",
"x$	c #F3F0F0",
"y$	c #FFFDFD",
"z$	c #F4F3F4",
"A$	c #5C5C5C",
"B$	c #949294",
"C$	c #7F7E7E",
"D$	c #D3D1D1",
"E$	c #B9B7B7",
"F$	c #E7E5E5",
"G$	c #F3F1F1",
"H$	c #F3E5E7",
"I$	c #F58A9E",
"J$	c #F7244B",
"K$	c #F1163D",
"L$	c #F4103A",
"M$	c #E87E92",
"N$	c #F6E9EB",
"O$	c #F4F2F2",
"P$	c #DFDEDE",
"Q$	c #B7B5B5",
"R$	c #C9C8C8",
"S$	c #E1E0E0",
"T$	c #CFCFCF",
"U$	c #838283",
"V$	c #969696",
"W$	c #DEDBDB",
"X$	c #F2EEEE",
"Y$	c #F3E6E8",
"Z$	c #F58B9E",
"`$	c #F72A50",
" %	c #F5C4CD",
".%	c #F4BFC8",
"+%	c #F5284E",
"@%	c #F6F4F4",
"#%	c #EAE8E8",
"$%	c #9C9B9B",
"%%	c #686868",
"&%	c #868686",
"*%	c #646564",
"=%	c #959395",
"-%	c #919191",
";%	c #C2C1C1",
">%	c #F7F5F5",
",%	c #F4E7E9",
"'%	c #F58B9F",
")%	c #F4C3CB",
"!%	c #C9C7C7",
"~%	c #E8B7C1",
"{%	c #F68C9F",
"]%	c #F5E8EA",
"^%	c #F5F3F3",
"/%	c #FAF8F8",
"(%	c #6D6C6C",
"_%	c #CBC9CB",
":%	c #6F6F6F",
"<%	c #434343",
"[%	c #8F8E8F",
"}%	c #929192",
"|%	c #636262",
"1%	c #F5DDE1",
"2%	c #F67990",
"3%	c #F2C1CA",
"4%	c #D2D0D1",
"5%	c #E4E2E2",
"6%	c #B47F89",
"7%	c #EF2248",
"8%	c #CCCCCC",
"9%	c #E8E7E8",
"0%	c #939293",
"a%	c #8C8C8C",
"b%	c #4B4B4B",
"c%	c #AFAEAE",
"d%	c #F9F7F7",
"e%	c #F65F7A",
"f%	c #F6C5CE",
"g%	c #E3E1E1",
"h%	c #C2C0C0",
"i%	c #CBC8C9",
"j%	c #9C9394",
"k%	c #B0ACB0",
"l%	c #E8B8C1",
"m%	c #F68CA0",
"n%	c #F5E9EB",
"o%	c #F6F5F5",
"p%	c #9A9999",
"q%	c #E6E6E6",
"r%	c #F6D2D8",
"s%	c #F72B51",
"t%	c #F6C6CF",
"u%	c #F6F5F4",
"v%	c #E9E7E8",
"w%	c #E2E0E0",
"x%	c #A9A7A7",
"y%	c #9D9595",
"z%	c #AAA7AB",
"A%	c #F3F2F2",
"B%	c #F73156",
"C%	c #F7D3D9",
"D%	c #484747",
"E%	c #B4B4B4",
"F%	c #B1AFB1",
"G%	c #8D8C8D",
"H%	c #8A898A",
"I%	c #999999",
"J%	c #F8C7D0",
"K%	c #F7C6CF",
"L%	c #9B9898",
"M%	c #A39FA2",
"N%	c #F7F6F6",
"O%	c #F7C7CF",
"P%	c #F6C7CF",
"Q%	c #6E6E6E",
"R%	c #C8C6C8",
"S%	c #3E3E3E",
"T%	c #7E7D7E",
"U%	c #F3163F",
"V%	c #F71640",
"W%	c #E5E4E4",
"X%	c #D8D3D4",
"Y%	c #EAE9E9",
"Z%	c #E9E8E8",
"`%	c #ECBCC5",
" &	c #F53659",
".&	c #916E74",
"+&	c #C2C0C2",
"@&	c #2A2A2A",
"#&	c #868586",
"$&	c #8C8B8C",
"%&	c #7A7B7A",
"&&	c #BEB3B5",
"*&	c #F28096",
"=&	c #F81D46",
"-&	c #F80D38",
";&	c #F82B51",
">&	c #F8A1B1",
",&	c #F8F7F7",
"'&	c #A7A7A7",
")&	c #A8A7A7",
"!&	c #ABA9AA",
"~&	c #B6B7B7",
"{&	c #BBBABA",
"]&	c #BEBDBD",
"^&	c #D0A0A9",
"/&	c #E84D69",
"(&	c #F5143E",
"_&	c #F03356",
":&	c #B05D6D",
"<&	c #564A4C",
"[&	c #B5B5B5",
"}&	c #DADADA",
"|&	c #C6C5C6",
"1&	c #323232",
"2&	c #605F60",
"3&	c #949494",
"4&	c #B1B0B0",
"5&	c #F9F8F8",
"6&	c #EEECEC",
"7&	c #E3E2E2",
"8&	c #FFFFFF",
"9&	c #F4F3F3",
"0&	c #F1F0F0",
"a&	c #EFEEEE",
"b&	c #F0EFEF",
"c&	c #4D4D4D",
"d&	c #D7D5D7",
"e&	c #A2A1A2",
"f&	c #848384",
"g&	c #898989",
"h&	c #FFFEFE",
"i&	c #FDFCFC",
"j&	c #C5C4C4",
"k&	c #EBEAEA",
"l&	c #FAF9F9",
"m&	c #878787",
"n&	c #BBBABB",
"o&	c #AAA9AA",
"p&	c #7D7C7D",
"q&	c #585757",
"r&	c #B7B6B6",
"s&	c #A9A8A8",
"t&	c #FAFAFA",
"u&	c #A3A2A2",
"v&	c #848484",
"w&	c #C9C9C9",
"x&	c #DAD9DA",
"y&	c #C0BFC0",
"z&	c #ACABAC",
"A&	c #747374",
"B&	c #8F9090",
"C&	c #B3B3B3",
"D&	c #FCFBFB",
"E&	c #FEFDFD",
"F&	c #BDBCBC",
"G&	c #B4B3B3",
"H&	c #797878",
"I&	c #858686",
"J&	c #B3B2B3",
"K&	c #9A999A",
"L&	c #333333",
"M&	c #6C6B6C",
"N&	c #929191",
"O&	c #9B9A9A",
"P&	c #949393",
"Q&	c #8D8D8D",
"R&	c #BCBCBC",
"S&	c #B1B0B1",
"T&	c #454545",
"U&	c #6E6D6E",
"V&	c #777777",
"W&	c #B0B0B0",
"X&	c #9E9E9E",
"Y&	c #A09FA0",
"Z&	c #4D4C4D",
"`&	c #616061",
" *	c #6B6A6B",
".*	c #959495",
"+*	c #AEAEAE",
"@*	c #585858",
"#*	c #424242",
"$*	c #706F70",
"%*	c #767576",
"&*	c #818081",
"**	c #6D6D6D",
"=*	c #5F5E5F",
"-*	c #2F2F2F",
";*	c #3D3D3D",
">*	c #474747",
",*	c #5A5A5A",
"'*	c #525252",
"                                                                                  .             ",
"                                                                            + @ # $ % &         ",
"                                                                          * =           - ;     ",
"                                      > , ' ) ! ~ {                       .               ]     ",
"                              ^ / ( _ : < [ } < | 1 2 3 4                 5 % 6           7 8   ",
"                        9 0 a b | c d e f g g h i j k l < m n o         p q r s t           ]   ",
"                      u v i w x g y z A B C D E F G H I J K [ L M N       O P Q R S       T U   ",
"                  V W X < Y b Z `  ...+.@.#.$.%.&.*.=.-.* % ;.| >.,.'.  ).!.~.{.].^./.    (._.  ",
"                @.:.<.[.f }.|.1.2.3.4.5.6.7.8.9.0.3.a.b.c.d.e.6 f.g.e h.i.^.j.k.l.8 m.n.> o.    ",
"              p.B | | # & q.r.s.t.u.v.w.x.y.z.u.A.B.C.D.E.F.G.^ H.I.J.K.L.M.N.O.P.Q.R.V S.      ",
"            T.n U.x 6 V.W.X.Y.Z.`. +.+++@+#+ +$+%+&+*+=+-+;+>+,+'+W )+!+< h.~+{+  ]+            ",
"          ^+/+(+_+:+<+[+}+|+k.1+2+3+4+5+6+3+7+8+5+k.9+9+0+a+b+c+d+e+u e f+g+u                   ",
"          h+i+j+k+l+m+n+o+p+q+r+s+t+t+t+t+t+t+t+t+t+t+u+v+w+~.a+x+y+z+A+B+K C+D+                ",
"        , E+F+G+'.H+I+o+J+J+K+L+M+N+O+O+O+O+O+O+O+O+O+O+P+Q+R+S+T+U+V+W+X+Y+>.Z+`+              ",
"       @.@g +@W @@#@p+J+J+J+J+$@%@&@*@O+O+O+O+O+O+O+O+O+=@-@;@J+J+>@,@'@)@!@~@{@]@^@            ",
"      ' E+/@(@d._@*@:@o+J+J+J+J+<@[@}@|@1@1@1@1@1@1@1@2@3@4@J+J+J+J+5@6@8.-.7@8@9@0@a@          ",
"    b@+ # ] c@d@e@1@f@g@J+J+J+J+J+h@i@j@1@1@1@1@1@1@2@k@<@J+J+J+J+l@m@n@o@$ p@q@r@b@            ",
"    s@z y t@u@O+v@1@1@w@x@J+J+J+J+J+y@z@A@1@1@1@1@2@B@<@J+J+J+J+J+x@C@D@E@p@F@G@H@I@s@          ",
"    J@K@r L@w.M@v@N@N@N@O@x@J+J+J+J+J+P@Q@R@S@S@R@T@<@J+J+J+J+J+x@U@V@W@X@q@Y@G@Z@`@0           ",
"  b@e. #U .#7+M@+#S@S@R @###$#J+J+J+J+J+%#&#*#*#&#<@J+J+J+J+J+x@=#V@-#p@q@;#H@>#,#,#;           ",
"  9 & Z+'#)#!#~#].N@{#]#^#/#(#_#J+J+J+J+J+p+:#:#<@J+J+J+J+J+x@<#[#W@p@F@}#|#1.N 1#[.X ~         ",
"  '+2#& 4 3#+#+#4#5#6#7#N@8#9#0#x@J+J+J+J+J+a#a#J+J+J+J+J+x@b#V@W@X@q@c#8@>+|@d#e#K f#g#        ",
"  h#i#+ j#k#l#l#m#l#O+n#S@o#p#q#0#x@J+J+J+J+J+J+J+J+J+J+x@r#s#E@t#{ c#(.'@u#v#w#U f+Y+L.        ",
"  y#& z#S.A#B#B#B#B#S@C#v@l#M@D#N@E#x@J+J+J+J+J+J+J+J+x@E#F#e G#H#I#.#J#K#L#M#N#O#< f+- '+      ",
"  S#T#U#D+V#W#W#W#W#W#X#Y#N@N@B#Z#`# $.$J+J+J+J+J+J++$@$5#V##$$$%$&$*$=$-$-$;$>$ .,$'$)$!$      ",
"  ^$/$O#+.($-$-$-$-$-$-$-$^#_$-$-$:$<$[$J+J+J+J+J+J+$@&#}$|$1$2$3$4$5$6$6$6$7$8$0 ,$g.9$        ",
"  V c$d$! k#-$-$-$-$-$-$-$6$6$-$:$<$p+J+J+J+J+J+J+J+J+e$f$g$h$i$j$k$6$6$6$6$l$I#m$f+n$o$        ",
"  9@p$q$+.r$=$=$K#K#K#K#K#K#K#s$t$p+J+J+J+J+J+J+J+J+J+J+e$u$v$]#w$x$x$x$x$x$y$8@C z$y A$        ",
"  F@B$ .g#C${#D$E$F$G$x$x$x$H$I$p+J+J+J+J+J+J$K$J+J+J+J+J+L$M$N$G$G$O$P$Q$R$S$ @T$z$c$>#        ",
"  r@U$V$o >#[#W$X$w$x$x$x$Y$Z$p+J+J+J+J+J+`$ %.%+%J+J+J+J+J+p+Z$Y$G$G$O$@%#%$%%%8 8 &%          ",
"    *%=%-%e+;%>%O$O$O$O$,%'%p+J+J+J+J+J+`$)%P$!%~%$#J+J+J+J+J+p+{%]%^%^%^%/%(%W f+_%:%          ",
"    <%[%}%d.|%@%^%^%^%1%2%[$J+J+J+J+J+`$3%4%5%I.1$6%7%J+J+J+J+J+p+{%]%^%^%^.#.8%9%z##.          ",
"    R#S#0%a%b%c%d%^%]%e%K+J+J+J+J+J+`$f%g%h%8 i%j%k%l%`$J+J+J+J+J+p+m%n%o%p%D+q%] =.            ",
"      q@h.4 ^ c.N@@%r%<@J+J+J+J+J+s%t%u%v%w%x%y%z%`#A%t%s%J+J+J+J+J+B%C%G$D%E%z F%@.            ",
"      R#@.G%H%N I%>%J%J+J+J+J+J+s%K%>%A%m D L%M%W#G$K#N%O%s%J+J+J+J+J+P%Q%L@A R%L.              ",
"        S%T%h.S. @H.C%U%J+J+J+V%K%>%>%W%U#B X%5+Y%W%Z%#%Y%`%x@o+J+J+ &.&I#W@f.+&S.              ",
"        @&9 #&$&%&D%&&*&=&-&;&>&,&,&,&'&!@- )&!&!@E%~&{&]&;%^&/&(&_&:&<&[&}&|&I@                ",
"          1&2&A+3&S.9 4&M#/%d%d%d%5&6&7&8&y$/%9&0&Y%Y%a&a&b&b&A%Y%d$c&= g+d&e&~                 ",
"            &$.#f&I%A+T.g&`#h&5&5&i&j&k&l&5&5&l&l&7#O l&l&l&8&F#m&#.n&(+E+o&^                   ",
"              G@I#p&|.V$*.q&r&5&l&8&s&8&l&l&l&l&l&t&B t&t&o%u&@.v&w&x&y&z&!                     ",
"                ;#A$A&V.O#B&#.L@C&E@= i&D&h&h&E&D&l&F&P.G&H&I&m$6 K@J&K&s@                      ",
"                  L&e+M&p&X+{@|.g#c@g#5 N&O&U#0 P&Q&4 I@-%t@R&M S&3 o.h#                        ",
"                    @&T&#.U&V&3&W&C&+ O#0  . .X&'&H.E%H.n z&X&Y&3&Q%                            ",
"                        &$Z&`& *y#f&.*U#* H.H.+*o&. q$W }%}%m&! @*                              ",
"                            &$#*{ .#$*%*c@T%&*f&Z@o.A+D+**=*>                                   ",
"                                  -*;*>*> @*,*T.p@'*>*S%                                        ",
"                                                                                                ",
"                                                                                                "]



class options:
    """Handles the options from argv and rc file"""

    def __init__( self, optFile=None ):
        self.options = {}
        self.optFile = optFile

    def addOpt( self, opt ):
        """Adds valid options and their default value"""
        keys = opt.keys()
        for k in keys:
            self.options[k] = opt[k]

    def getOpt( self, key, kind=None ):
        """Get values for options"""
        keys = self.options.keys()
        if key in keys:
            if kind != None:
                if self.options[key] == []:
                    print '%s needs a value specified' % key
                    ErrorWindow( 'option %s needs a value specified' % key )
                    sys.exit()
                if kind == 'int':
                    ret = []
                    for o in self.options[key]:
                        try:
                            ret.append( int(o) )
                        except ValueError:
                            print '%s is not a valid value for %s, have to be integer' % (o, key)
                            ErrorWindow( '%s is not a valid value for %s, have to be integer' % (o, key) )
                            sys.exit()
                    return ret
                if kind == 'str': # they are strings already, but do this for symmetry with abow
                    ret = []
                    for o in self.options[key]:
                        try:
                            ret.append( str(o) )
                        except ValueError:
                            print '%s is not a valid value for %s, have to be string' % (o, key)
                            ErrorWindow( '%s is not a valid value for %s, have to be string' % (o, key) )
                            sys.exit()
                    return ret
                else:
                    return self.options[key]
            return self.options[key]
        else:
            return False

    def updateValues( self ):
        """Updates the global options"""
        if options == None or options == {}:
            return

        rcLst = []            
        #make list from options in rc file
        if self.optFile != None:
            try:
                fh = open( self.optFile, 'r' )
                for line in fh.readlines():
                    if len(line.strip()) > 0 and line.strip()[0] == '#':
                        continue
                    for arg in line[:-1].split():
                        rcLst.append( arg )
                fh.close()
            except IOError:
                pass

        #get values from rc
        self.parseList( rcLst )

        #get values from argv
        self.parseList( sys.argv[1:] )

    def parseList( self, lst ):
        """update self.options from a list"""
        if len( lst ) < 1:
            return
        opt = None
        for i in range( len( lst ) ):
            if lst[i] in self.options.keys():
                opt = lst[i]
                self.options[opt] = []
            else:
                if opt != None:
                    self.options[opt].append( lst[i] )

    def printOpts( self ):
        """Prints options and their values"""
        keys = self.options.keys()
        keys.sort()
        for key in keys:
            params = ''
            if self.options[key] == None:
                params = ' Not specified'
            elif self.options[key] == []:
                params = ' No parameters'
            else:
                for p in self.options[key]:
                    params = params + ' ' + str(p)
            print '%-8s%s' % (key, params)


class MousePointer:
    """Checks where the mouse pointer is and if it has moved"""
    def __init__( self ):
        self.display = Xlib.display.Display()
        self.screen  = self.display.screen()
        self.root    = self.screen.root
        self.ox, self.oy = self.getPointer()
        
    def getPointer( self ):
	info = Xlib.protocol.request.QueryPointer( display = self.display.display, window = self.root)
        x = info._data['root_x']
        y = info._data['root_y']
	return (x, y)

    def hasMoved( self ):
        x, y = self.getPointer()
        moved = False
        if ( self.ox != x ) or ( self.oy != y ):
            moved = True
        self.ox, self.oy = x, y
        return moved


class Communicator:
    """Communicates with other insances to detect work on other computers"""
    def __init__( self, mode, port, host=None ):
        self.mode               = mode
        self.serverSocket       = None
        self.connectedSockets   = []
        self.clientSocket       = None
        self.clientIsConnected  = False
        self.host               = None
        self.port               = port
        self.theMessage         = 'all work and no play..'
        if self.mode == 'server':
            self.serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            if host == None:
                addresses = socket.gethostbyname_ex(socket.gethostname())[2]
                for addr in addresses:
                    if addr[:3] != '127':
                        self.host = addr                   # use ip address
                if self.host == None:
                    self.host = socket.gethostname()       # use hostname
            else:
                self.host = host                           # use supplied addr/name
            print 'binding to %s:%u' % (self.host, self.port)
            try:
                self.serverSocket.bind( (self.host, self.port) )
            except socket.error:
                print 'Could not bind to %s:%u. Address already in use?' % (self.host, self.port)
                ErrorWindow( 'Could not bind to %s:%u. Address already in use?' % (self.host, self.port) )
                sys.exit()
            self.serverSocket.listen( 5 )
        else: # client
            if host == None:
                print 'Client need a host to connect to'
                ErrorWindow( 'Client need a host to connect to' )
                sys.exit()
            self.host = host
            self.clientSocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

    def connect( self ):
        if self.mode == 'server':
            readers, writers, errs = select.select([self.serverSocket], [], [], 0.1)
            while self.serverSocket in readers:
                (clientsocket, address) = self.serverSocket.accept()
                self.connectedSockets.append( clientsocket )
                readers, writers, errs = select.select([self.serverSocket], [], [], 0.1)
            return
        else: # client
            readers, writers, errs = select.select([], [self.clientSocket], [], 0.1)
            if self.clientSocket in writers:
                try:
                    self.clientSocket.connect( (self.host, self.port) )
                    self.clientIsConnected = True
                    return
                except socket.error:
                    return
            

    def isConnected( self ):
        if self.mode == 'server':
            return len( self.connectedSockets ) > 0
        else: # client
            return  self.clientIsConnected

    def getNrConnected( self ):
        return len( self.connectedSockets )

    def msg( self ):
        # clients send, server receive
        if self.mode == 'server':
            # check for new clieants
            self.connect()
            
            newMsg = False
            if self.connectedSockets == []:
                return False
            readers, writers, errs = select.select( self.connectedSockets, [], [], 0.1 )
            for s in self.connectedSockets:
                if s in readers: # also true if client is disconnected
                    msg = ''
                    while len(msg) < len(self.theMessage):
                        chunk = s.recv(len(self.theMessage)-len(msg)) # here we may actually hang..
                        if chunk == '':
                            # client has left
                            self.connectedSockets.remove( s )
                            break
                        msg = msg + chunk
                    newMsg = True
            return newMsg
        else: # client
            if not self.clientIsConnected:
                self.connect()
                if not self.clientIsConnected:
                    return False  # don't get stuck here
        
            readers, writers, errs = select.select([], [self.clientSocket], [], 0.1)
            if self.clientSocket in writers:
                totalsent = 0
                while totalsent < len(self.theMessage):
                    try:
                        sent = self.clientSocket.send( self.theMessage[totalsent:] )
                    except socket.error:
                        self.clientSocket.close()
                        self.clientIsConnected = False
                        self.clientSocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                        return False
                    if sent == 0:
                        # socket connection broken
                        self.clientSocket.close()
                        self.clientIsConnected = False
                        self.clientSocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
                        return False
                    totalsent = totalsent + sent
                return True
            else:
                return False

    def close( self ):
        try:
            if self.mode == 'server':
                self.serverSocket.shutdown( socket.SHUT_RDWR )
                self.serverSocket.close()
            else:
                self.clientSocket.shutdown( socket.SHUT_RDWR )
                self.clientSocket.close()
        except socket.error:
            pass


class DayTimesRecord:
    """Stores the times for one day. These records are stored in the DB"""
    def __init__( self ):
        self.times       = []     # the time element is stored as ints (minutes): [came, left]
        self.autoLunch   = True   # indicates if time for lunch should automatically be removed
        self.lunchActive = False

class DataBase:
    """Stores records for all day times"""
    def __init__( self ):
        self.records = {}

    ## low level, methods to handle the records
    def new( self, date ):
        key = date[0] * 10000 + date[1] * 100 + date[2]
        self.records[key] = DayTimesRecord()

    def remove( self, date ):
        key = date[0] * 10000 + date[1] * 100 + date[2]
        del self.records[key]

    def get( self, date ):
        key = date[0] * 10000 + date[1] * 100 + date[2]
        if not key in self.records.keys():
            return None
        return self.records[key]

    def set( self, date, record ):
        key = date[0] * 10000 + date[1] * 100 + date[2]
        self.records[key] = record

    def getToday( self ):
        date = time.localtime()[:3]
        key = date[0] * 10000 + date[1] * 100 + date[2]
        if not key in self.records.keys():
            return None
        return self.records[key]

    def has( self, date ): 
        key = date[0] * 10000 + date[1] * 100 + date[2]
        return key in self.records.keys()

    ## higher level, methods to handle the records
    def setIsWorking( self ):
        # working is always done "now"
        date = time.localtime()[:3]
        if not self.has( date ):
            self.new( date )
        # add a new time if it is an empty record 
        if self.get( date ).times == []:
            self.addTime( date, 'now' )
            self.updateLunch( date )
            return
        # check last known time vs now
        record = self.get( date )
        times  = record.times
        left = times[len(times) - 1][1]
        h, m = time.localtime()[3:5]
        now = (h * 60) + m
        # add new time if we have been idle long enough
        if (now > left) and ((now - left) > param.idleBreakTime):
            self.addTime( date, 'now' )
            self.updateLunch( date )
            return
        # update last known time
        times[len(times) - 1][1] = now
        record.times = times
        self.set( date, record )
        self.updateLunch( date )

    def addTime( self, date, t ):
        record = self.get( date )
        if t == 'now':
            h, m = time.localtime()[3:5]
            now = (h * 60) + m
            came = left = now
            record.times.append( [came, left] )
        else:
            record.times.append( t )
        self.set( date, record )

    def getTimesAsStr( self, date ):
        string = ''
        i = 0
        for t in self.get( date ).times:
            if i > 0:
                string = string + ', '
            cameH = t[0]/60
            cameM = t[0]%60
            leftH = t[1]/60
            leftM = t[1]%60
            string = string + '%02d.%02d-%02d.%02d' % (cameH, cameM, leftH, leftM)
            i += 1
        return string

    def setTimesFromStr( self, date, times ):
        if times.lstrip() == '':
            self.new( date ) # new = clear
            return True
        # first check all times
        if ',' in times:
            times = times.split( ',' )
        else:
            times = [times]
        try:
            tot = 0
            for t in times:
                if not '-' in t:
                    return False
                came, left = t.split( '-' )
                if '.' in came:
                    cameH, cameM = came.split( '.' )[0:2]
                else :
                    cameH, cameM = (came, 0)
                if '.' in left:
                    leftH, leftM = left.split( '.' )[0:2]
                else :
                    leftH, leftM = (left, 0)
                c = int(cameH)*60 + int(cameM)
                l = int(leftH)*60 + int(leftM)
                if l < c:
                    return False
                tot += l - c
            if tot > 1440:
                return False
        except ValueError:
            return False
        # all times are ok, use them
        self.new( date ) #new = clear
        try:
            for t in times:
                came, left = t.split( '-' )
                if '.' in came:
                    cameH, cameM = came.split( '.' )[0:2]
                else :
                    cameH, cameM = (came, 0)
                if '.' in left:
                    leftH, leftM = left.split( '.' )[0:2]
                else :
                    leftH, leftM = (left, 0)
                c = int(cameH)*60 + int(cameM)
                l = int(leftH)*60 + int(leftM)
                self.addTime( date, [c, l] )
            self.updateLunch( date )
            return True
        except ValueError:
            return False

    def getTotTime( self, date ):
        """Returns an int with total time as minutes."""
        tot = 0
        for t in self.get( date ).times:
            tot += (t[1] - t[0])    # left - came
        return tot

    def getTotTimeLunch( self, date ):
        """Returns an int with total time as minutes. Time is adjusted for lunch."""
        tot = self.getTotTime( date )
        if self.get( date ).lunchActive:
            tot = max( 0, tot - param.lunchLength )
        return tot

    def getTotTimeLunchAsString( self, date ):
        tot = max( 0, self.getTotTimeLunch( date ) )
        totStr = ''
        if tot != 0:
            h      = tot/60
            # display minutes as 1/100 or 1/60 of hours
            m      = tot%60 * param.totTimeMinuteDevider / 60
            totStr = '%2d%s%02d'%(h,param.totTimeDelimiter,m)
        return totStr
        
    def setTotTime( self, date, newTot ):
        """Updates the record so that the sum is as the specified newTot"""
        # if autoLunch is active the time may be reduced automatically.
        # this is probably not what is intended so add time for lunch in that case.
        # the same applies when lunch is set to active ('yes')
        oldTot = self.getTotTime( date )
        record = self.get( date )
        lunchState = self.getLunchState( date )
        if (lunchState == 'auto' and (newTot > param.lunchAfterMin)) or lunchState == 'yes':
            newTot += param.lunchLength
        if record.times == []:
            record.times = [[0, 0]]
        times = record.times
        if newTot > oldTot:
            # add time to the last time element
            addedTime = newTot - oldTot
            orgLeft = times[ len(times) - 1 ][1]
            times[len(times)-1][1] = min( 1440, orgLeft + addedTime )
        elif newTot < oldTot:
           timeToRemove = oldTot - newTot
           for i in range( len(times)-1, -1, -1 ):
               t        = times[i]
               thisTime = t[1] - t[0]
               if thisTime <= timeToRemove:
                   #remove the whole time entry
                   timeToRemove -= thisTime
                   del(times[i])
               else:
                   #decrease time entry and stop 
                   times[i][1] = times[i][1] - timeToRemove
                   break
        record.times = times
        self.set( date, record )
        self.updateLunch( date )

    def setTotTimeFromStr( self, date, text ):
        try:
            if param.totTimeDelimiter in text:
                h, m = text.split( param.totTimeDelimiter )
                h    = min( 24, int(h) )
                m    = min( (param.totTimeMinuteDevider -1), int(m) )
                tot  = min( 1440, h*60+(m*60/param.totTimeMinuteDevider)) 
                self.setTotTime( date, tot )
            else:
                tot  = min( 24, int(text) ) * 60
                self.setTotTime( date, tot )
        except ValueError:
            return

    def getLunch( self, date ):
        return self.get( date ).lunchActive

    def getLunchState( self, date ):
        """Returns the state of the lunch. 'yes', 'no', 'auto'"""
        record = self.get( date )
        if record.autoLunch:
            return 'auto'
        if record.lunchActive:
            return 'yes'
        else:
            return 'no'
 
    def setLunchState( self, date, newState ):
        """This one is called when lunch is to be set to a specific value (input is 'yes', 'no', 'auto')"""
        record = self.get( date )
        if newState.lower() == 'auto':
            record.autoLunch = True
            self.set( date, record )
            self.updateLunch( date )
            return
        if newState.lower() == 'yes':
            record.lunchActive =  True
            record.autoLunch = False
        elif newState.lower() == 'no':
            record.lunchActive =  False
            record.autoLunch = False
        self.set( date, record )

    def updateLunch( self, date ):
        record = self.get( date )
        if record.autoLunch:
            if self.getTotTime( date ) > param.lunchAfterMin:
                record.lunchActive = True
            else:
                record.lunchActive = False
        self.set( date, record )

    def getSummaryTime( self, date1, date2=None ):
        if date2 == None:
            date2 = date1
        if date2 < date1:
            return None               # error, we don't support calculating backwards
        date = date1
        sum = 0
        while date <= date2:
            tot = self.getTotTimeLunch( date )
            if 'S' in getDayName( date ): #saturday or sunday
                sum += tot
                date = getNextDay( date )
                continue
            if tot == 0:                  # no work done.. don't calculate summary
                date = getNextDay( date )
                continue
            sum += tot - param.workDayLength
            date = getNextDay( date )
        return sum

    def getSummaryTimeAsString(self, date1, date2=None ):
        sum = self.getSummaryTime( date1, date2 )
        if sum == 0:
            return ''
        if sum < 0:
            s = '-'
            sum = sum * -1
        else:
            s = ' '
        h    = sum/60
        m    = sum%60
        return '%s%02d.%02d' % (s, h, m)



def getDayName( date ):
    """Returns the (two letter) name of a date"""
    y, m, d = date
    s = time.mktime( (y, m, d, 0, 0, 0, 0, 0, 0) )
    dayNum = time.localtime( s )[6]
    if dayNum == 0: return 'Mo'
    if dayNum == 1: return 'Tu'
    if dayNum == 2: return 'We'
    if dayNum == 3: return 'Th'
    if dayNum == 4: return 'Fr'
    if dayNum == 5: return 'Sa'
    if dayNum == 6: return 'Su'

def getNextDay( date ):
    """Returns the day after date"""
    y, m, d = date
    s = time.mktime( (y, m, d, 0, 0, 0, 0, 0, 0) )
    s += 86400                            # add one day
    return time.localtime( s )[:3]


def getMonthFromFile( thisMonth='now', dir=None ):
        """Get the days of a month from file."""
        if thisMonth == 'now':
            year, month = time.localtime()[:2]
        else:
            year, month = thisMonth
        if dir == None:
            dir = param.dataDir
        if not os.path.isabs( dir ):
            dir = os.getcwd() + '/' + dir
        filePath = '%s/%04d-%02d' % (dir, year, month)
        validDates = datesInMonth( year, month )
        # get the days into the db
        for date in validDates:
            if not db.has( date ):
                db.new( date )
        # get data from file, if available
        if not os.path.exists( filePath ):
            return False
        try:
            fh = open( filePath, 'r')
            for line in fh.readlines():
                if not ': ' in line:
                    continue
                lunch = 'auto'
                # cut out lunch and remove newline from end of line
                if 'lunch=' in line:
                    line, lunch = line[:-1].split( 'lunch=' )
                else:
                    line = line[:-1]
                day, times = line.split( ': ' )
                date = (year, month, int(day))
                if not date in validDates:
                    continue
                db.setTimesFromStr( date, times )
                db.setLunchState( date, lunch )
        except:
            print 'Error reading log for month %04d-%02d' % (year, month)
        fh.close()
        return True

def saveMonthToFile( thisMonth='now', dir=None ):
    if thisMonth == 'now':
        year, month = time.localtime()[:2]
    else:
        year, month = thisMonth
    if dir == None:
        dir = param.dataDir
    if not os.path.isabs( dir ):
        dir = os.getcwd() + '/' + dir
    if not os.path.exists( dir ):
        os.mkdir( dir )
    filePath = '%s/%04d-%02d' % (dir, year, month)
    try:
        fh = open( filePath, 'w')
        for date in datesInMonth( year, month ):
            if not db.has( date ):
                continue
            line = '%2d: %s' % (date[2], db.getTimesAsStr( date ))
            lunch = db.getLunchState( date )
            if lunch != 'auto':
                line += ' lunch=%s\n' % lunch
            else:
                line += '\n'
            fh.write( line )
    except:
        print 'Error writing log for month %04d-%02d' % (year, month)
    fh.close()

def datesInMonth( year, month ):
    dates = []
    for day in range(1,32):
        s = time.mktime( (year, month, day, 0, 0, 0, 0, 0, 0) )
        m = time.localtime( s )[1]
        if m != month: # this day belongs to next month
            break
        dates.append( (year, month, day) )
    return dates

# info: the first/last week of a year belogs to the year where it has >= 4 days.
##
def dateToWeek( year, month, day ):
    s       = time.mktime( (year, month, day, 0, 0, 0, 0, 0, 0) )
    yearDay = time.localtime( s )[7]
    weekOneFirstDay, weekOneLastDay = daysInWeekOne( year )
    if yearDay < weekOneFirstDay:
        # last week of previous year i.e. week "0"
        return 0
    elif yearDay <= weekOneLastDay:
        return 1
    else:
        return ((yearDay - weekOneLastDay - 1) / 7 ) + 2

def datesInWeek( year, week, onlyFirst=False, num=1 ):
    secInDay = 60*60*24
    minWeek, maxWeek = weeksInYear( year )
    if week < minWeek or week > maxWeek:
        return []
    weekOneFirstDay, weekOneLastDay = daysInWeekOne( year )
    lastYearDayOfWeek = (week - 1) * 7 + weekOneLastDay                    # the last day in week (Su) is always in this year..
    firstSecOfYear = time.mktime( (year, 1, 1, 0, 0, 0, 0, 0, 0) )         
    firstSecOfWeek = firstSecOfYear + ((lastYearDayOfWeek - 7) * secInDay) # but the first second in week may be in previous year
    if num > 1:
        firstSecOfWeek -= (num - 1) * (7*24*60*60)
    dates = []
    for day in range(7*num):
        y, m, d = time.localtime( firstSecOfWeek + (day * secInDay))[:3]
        dates.append( (y, m, d) )
        if onlyFirst:
            break
    return dates

def daysInWeekOne( year ):
    for day in range( 1, 8 ):
        s  = time.mktime( (year, 1, day, 1, 1, 1, 0, 0, 0) )
        if time.localtime( s )[6] == 6: #sunday
            if day >= 4:
                # first days of year belong to week 1, i.e. no days before it
                return (1, day)
            else:
                # first days belong to week in previous year
                return (day, day+7)

def weeksInYear( year ):
    lastDayInYear = time.mktime( (year, 12, 31, 0, 0, 0, 0, 0, 0) )
    daysInYear    = time.localtime( lastDayInYear )[7]
    weekOneFirstDay, weekOneLastDay = daysInWeekOne( year )
    if weekOneFirstDay != 1: # there are days before week one
        firstWeek = 0
    else:
        firstWeek = 1
    daysWeekTwoToEnd   = daysInYear - weekOneLastDay
    wholeWeeksTwoToEnd = daysWeekTwoToEnd / 7
    # if the days left over is >= 4 also those are in a week for this year
    if (daysWeekTwoToEnd % 7) >= 4:
        lastWeek = 1 + wholeWeeksTwoToEnd + 1
    else:
        lastWeek = 1 + wholeWeeksTwoToEnd
    return (firstWeek, lastWeek)

class ListView:
    """Handles the view for displaying day records"""
    def __init__( self, db ):
        self.db = db
        self.ls   = gtk.ListStore( str, str, str, 'gboolean', str, str, str, 'gboolean')
        self.view = gtk.TreeView( self.ls )
        self.editingPath = None 
        # construct cell renderers
        self.crTxt0a = gtk.CellRendererText()
        self.crTxt0a.set_property( 'ypad', 0 )
        self.crTxt0b = gtk.CellRendererText()
        self.crTxt0b.set_property( 'ypad', 0 )
        self.crTxt1 = gtk.CellRendererText()
        self.crTxt1.set_property( 'ypad', 0 )
        self.crTxt1.set_property( 'ellipsize', pango.ELLIPSIZE_END )
        self.crTxt1.connect( 'editing-started', self.crEditingCb )
        self.crTxt1.connect( 'edited', self.crTxtCb, (self.ls, 1) )
        self.crTxt2 = gtk.CellRendererText()
        self.crTxt2.set_property( 'ypad', 0 )
        self.crTxt2.connect( 'editing-started', self.crEditingCb )
        self.crTxt2.connect( 'edited', self.crTxtCb, (self.ls, 2) )  
        self.crTog3 = gtk.CellRendererToggle()
        self.crTog3.set_property( 'ypad', 0 )
        self.crTog3.set_property( 'activatable', True )
        self.crTog3.connect( 'toggled', self.crTogCb, (self.ls, 3) )  
        self.crTxt4 = gtk.CellRendererText()
        self.crTxt4.set_property( 'ypad', 0 )
        # construct the columns
        self.col0 = gtk.TreeViewColumn( 'day' )
        self.col0.pack_start( self.crTxt0a, False )
        self.col0.pack_start( self.crTxt0b, False )
        self.col0.set_attributes( self.crTxt0a, text=6 )
        self.col0.set_attributes( self.crTxt0b, text=0 )
        self.col1 = gtk.TreeViewColumn( 'times', self.crTxt1, text=1,   editable=7 )
        self.col1.set_expand( True )
        self.col2 = gtk.TreeViewColumn( 'tot',   self.crTxt2, text=2,   editable=7 )
        self.col3 = gtk.TreeViewColumn( 'lunch', self.crTog3, active=3, visible=7 )
        self.col4 = gtk.TreeViewColumn( 'sum',   self.crTxt4, text=4 )
        # append columns to the view
        self.view.append_column( self.col0 )
        self.view.append_column( self.col1 )
        self.view.append_column( self.col2 )
        if param.showLunch:
            self.view.append_column( self.col3 )
        self.view.append_column( self.col4 )
        self.view.show()

    def dateToStr( self, date ):
        return '%04d%02d%02d'%date

    def strToDate( self, string ):
        y = int(string[:4])
        m = int(string[4:6])
        d = int(string[6:8])
        return (y, m, d)

    def populate( self, dates, sumInterval=None):
        if dates == []:
            return
        self.ls.clear()
        i = 1
        for date in dates:
            if not db.has( date ):
                getMonthFromFile( date[0:2] )
            name  = getDayName( date )
            day   = date[2]
            times = db.getTimesAsStr( date )
            tot   = db.getTotTimeLunchAsString( date )
            lunch = db.getLunch( date )
            sum   = db.getSummaryTimeAsString( date )
            ds    = self.dateToStr( date )
            self.ls.append([day, times, tot, lunch, sum, ds, name, True])
            if sumInterval != None and i%sumInterval == 0:
                date1 = dates[i-sumInterval]
                date2 = dates[i-1]
                totSum = db.getSummaryTimeAsString( date1, date2 )
                self.ls.append(['', '', '', False, totSum, '', '', False])
            i += 1
        if sumInterval == None:
            date1  = dates[0]
            date2  = dates[len(dates)-1]
            totSum = db.getSummaryTimeAsString( date1, date2 )
            self.ls.append(['', '', '', False, totSum, '', '', False])

    def getView( self ):
        return self.view

    def crTogCb( self, cell, path, data ):
        model, col = data
        if not model[path][7]: # this is the summary entry
            return
        date = self.strToDate( model[path][5] )
        lunchActive = db.getLunch( date )
        # toggle db
        if lunchActive:
            db.setLunchState( date, 'no' )
        else:
            db.setLunchState( date, 'yes' )
        # redraw columns
        self.redrawRow( model, path, date )

    def crEditingCb( self, cellrenderer, editable, path ):
        # this one is called when editing starts
        self.editingPath = path

    def crTxtCb( self, cell, path, text, data ):
        # this one is called when editing has finished
        self.editingPath = None 
        model, col = data
        if not model[path][7]:  # this is the summary entry
            return
        date = self.strToDate( model[path][5] )
        # update db
        if col == 1:
            db.setTimesFromStr( date, text )
        elif col == 2:
            # due to round off errors we don't want to update if text is the same
            if text != db.getTotTimeLunchAsString( date ) :
                db.setTotTimeFromStr( date, text )
        # redraw columns
        self.redrawRow( model, path, date )
        
    def redrawRow( self, model, path, date ):
        # don't update when user is editing this row
        if self.editingPath != None and int(path) == int(self.editingPath): 
            return
        model[path][1] = db.getTimesAsStr( date )
        model[path][2] = db.getTotTimeLunchAsString( date )
        model[path][3] = db.getLunch( date )
        model[path][4] = db.getSummaryTimeAsString( date )

    def redrawDate( self, date ):
        for row in range( len(self.ls) ):
            if self.ls[row][7] and (self.strToDate( self.ls[row][5] ) == date): #normal row and wanted date
                self.redrawRow( self.ls, row, date )
    
    def redrawSums( self ):
        found1 = False
        for row in range( len(self.ls) ):
            if self.ls[row][7] and not found1:                     # normal row
                date1  = self.strToDate( self.ls[row][5] )
                found1 = True
            if not self.ls[row][7]:                                # summary row
                date2  = self.strToDate( self.ls[row-1][5] )
                found1 = False
                self.ls[row][4] = db.getSummaryTimeAsString( date1, date2 )


class MainWindow:
    """Draws the main window"""
    def __init__( self ):
        self.window = gtk.Window( gtk.WINDOW_TOPLEVEL )
        self.window.set_title( "gtt" )
        self.window.set_border_width(10)

        pb = gtk.gdk.pixbuf_new_from_xpm_data( gtt48_xpm )
        self.window.set_icon_list( pb )
        self.window.connect("delete_event", self.deleteEventCb)
        self.window.connect("destroy", self.destroyCb)
        self.window.set_default_size( param.width, param.height )
        
        self.currentDate    = time.localtime()[:3]
        self.year, self.month, self.day = time.localtime()[:3]
        self.week           = dateToWeek( self.year, self.month, self.day )
        self.listView       = ListView( db )
        self.viewMonth      = True
        if param.initialView == 'week':
            self.viewMonth  = False
        self.scrolledWindow = None
        self.saveButton     = None
        self.openButton     = None
        self.ySpinButton    = None
        self.mwSpinButton   = None
        if param.isServer:
            self.numConClientsLabel = None
        self.initReady      = False
        self.initialWindowSetup()
        self.initReady      = True
        self.scrolledWindow.add_with_viewport( self.listView.getView() )
        self.uppdateDisplay()
        self.needToSave     = False
        self.nextSaveTime   = 0
        # This timer keeps the loop going.. it calls the chkStatus method every sec.
        self.timer = gobject.timeout_add( 1000, self.chkStatus )

    def initialWindowSetup( self ):
        # a vbox for whole main window
        vbox = gtk.VBox()
        # this hbox holds the button row at the top of window
        hbox = gtk.HBox()
        # save/open buttons
        button = gtk.Button( label= 'Save' , stock=gtk.STOCK_SAVE )
        button.connect("clicked", self.buttonClickedCb, button, 'save')
        button.show()
        self.saveButton = button
        hbox.pack_start( button, False )
        button = gtk.Button( label= 'Open' , stock=gtk.STOCK_OPEN )
        button.connect("clicked", self.buttonClickedCb, button, 'open')
        button.show()
        self.openButton = button
        hbox.pack_start( button, False )
        # month or week view
        button = gtk.Button( label= 'View' , stock=None )
        button.connect("clicked", self.buttonClickedCb, button, 'view')
        button.show()
        hbox.pack_end( button, False )
        # month and year choosers
        if self.viewMonth:
            adj = gtk.Adjustment(value=self.month, lower=1, upper=12, step_incr=1)
        else:
            minWeek, maxWeek = weeksInYear( self.year )
            adj = gtk.Adjustment(value=self.week, lower=minWeek, upper=maxWeek, step_incr=1)
        spinButton = gtk.SpinButton(adjustment=adj)
        spinButton.set_width_chars( 2 )
        spinButton.connect("activate", self.spinButtonActivateCb, spinButton, 'month')
        spinButton.connect("changed", self.spinButtonChangedCb, spinButton, 'month')
        spinButton.show()
        hbox.pack_end( spinButton, False )
        self.mwSpinButton = spinButton
        adj = gtk.Adjustment(value=self.year, lower=1902, upper=2037, step_incr=1)
        spinButton = gtk.SpinButton(adjustment=adj)
        spinButton.set_width_chars( 4 )
        spinButton.connect("activate", self.spinButtonActivateCb, spinButton, 'year')
        spinButton.connect("changed", self.spinButtonChangedCb, spinButton, 'year')
        spinButton.show()
        hbox.pack_end( spinButton, False )
        self.ySpinButton = spinButton
        # label for connected clients
        if param.isServer:
            label = gtk.Label( '   ' )
            label.show()
            hbox.pack_end( label, False )
            self.numConClientsLabel = label
        #
        hbox.show()
        # put top row and the time display in new vbox
        vbox = gtk.VBox()
        vbox.pack_start( hbox, False )
        vbox.show()
        sw = gtk.ScrolledWindow()
        #automatic scrollbars in both ways
        sw.set_policy( gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC )
        sw.show()
        vbox.pack_start( sw )
        self.scrolledWindow = sw
        self.window.add( vbox )
        self.window.show()
        
    def chkStatus( self ):
        """This one is called every one second"""
        clientHasMoved = False
        if param.isServer:
            clientHasMoved = com.msg()
            nrConStr = '   '
            if com.getNrConnected() > 0:
                nrConStr = '%-3u' % com.getNrConnected()
            self.numConClientsLabel.set_text( nrConStr )
        if mp.hasMoved() or clientHasMoved:
            today = db.getToday()
            if today == None:
                getMonthFromFile()
                today = db.getToday()
                if today == None:
                    print "error in db, can't find today"
                    ErrorWindow( "error in db, can't find today" )
                    sys.exit()
            db.setIsWorking()
            self.listView.redrawDate( time.localtime()[:3] )
            self.needToSave = True

        # save every 15min, if mouse pointer has moved
        if self.needToSave and (time.time() > self.nextSaveTime):
            saveMonthToFile()
            self.needToSave = False
            self.nextSaveTime = time.time() + 900
        # always update display when a new day begins
        if self.currentDate != time.localtime()[:3]:
            self.year, self.month, self.day = time.localtime()[:3]
            self.week = dateToWeek( self.year, self.month, self.day )
            self.updateSpinButtons() 
            self.uppdateDisplay()
            self.currentDate = time.localtime()[:3]
        # redraw summary also when mouse pointer isn't moving, so that it is updated directly
        # without need to move the mouse first
        self.listView.redrawSums()
        return True # so that timer keeps calling us.

    def uppdateDisplay( self ):
        if not self.initReady:
            return
        if self.viewMonth:
            self.saveButton.show()
            self.openButton.show()
            self.listView.populate( datesInMonth( self.year, self.month ) )
        else:
            self.saveButton.hide()
            self.openButton.hide()
            self.listView.populate( datesInWeek( self.year, self.week, num=param.numWeeksInView ), 7 )
        
    def spinButtonActivateCb( self, widget, entry, type ):
        pass

    def spinButtonChangedCb( self, widget, entry, type ):
        try:
            self.year  = int( self.ySpinButton.get_text() )
            if self.viewMonth:
                month = int( self.mwSpinButton.get_text() )
                if month > 0 and month < 13:
                    self.month = month
            else:
                week = int( self.mwSpinButton.get_text() )
                minWeek, maxWeek = weeksInYear( self.year )
                if week >= minWeek and week <= maxWeek:
                    self.week = week
            self.updateSpinButtons()
            self.uppdateDisplay()
        except ValueError:
            pass

    def updateSpinButtons( self ):
        if self.viewMonth:
            self.mwSpinButton.set_text( '%d' % self.month )
            adj = gtk.Adjustment(value=self.month, lower=1, upper=12, step_incr=1)
            self.mwSpinButton.set_adjustment( adj )
        else:
            self.mwSpinButton.set_text( '%d' % self.week )
            minWeek, maxWeek = weeksInYear( self.year )
            adj = gtk.Adjustment(value=self.week, lower=minWeek, upper=maxWeek, step_incr=1)
            self.mwSpinButton.set_adjustment( adj )

    def buttonClickedCb( self, widget, entry, type ):
        if type == 'view':
            self.viewMonth = not self.viewMonth
            self.updateSpinButtons() 
            self.uppdateDisplay()
            return
        if type == 'open':
            getMonthFromFile( [self.year, self.month] )
            self.uppdateDisplay()
        elif type == 'save':
            saveMonthToFile( [self.year, self.month] )

    def deleteEventCb( self, widget, event, data=None ):
        return False

    def destroyCb( self, widget, data=None ):
        print 'saving current month..'
        saveMonthToFile()
        print 'done'
        gtk.main_quit()


class ClientStatusIcon:
    """Draws the client status icon"""
    def __init__( self ):
        self.shouldReport     = True
        com.connect()
        self.reportPixbuf = gtk.gdk.pixbuf_new_from_xpm_data( gtt48_xpm )
        self.noReportPixbuf = gtk.gdk.pixbuf_new_from_xpm_data( gtt48NoReport_xpm )
        self.statIcon = gtk.StatusIcon()
        self.statIcon.set_from_pixbuf( self.reportPixbuf )
        self.statIcon.set_visible( True )
        self.statIcon.connect( "activate", self.leftClickCb )
        self.statIcon.connect( "popup_menu", self.rightClickCb )
        
        # This timer keeps the loop going.. it calls the chkStatus method every sec.
        self.timer = gobject.timeout_add( 1000, self.chkStatus )

        
    def chkStatus( self ):
        """This one is called every one second"""

        if mp.hasMoved() and self.shouldReport:
            # inform server
            com.msg()

        if self.shouldReport:
            if com.isConnected():
                self.statIcon.set_blinking( False )
                self.statIcon.set_tooltip( 'gtt - Connected to server' )
            else:
                self.statIcon.set_blinking( True )
                self.statIcon.set_tooltip( 'gtt - Not connected to server' )
        else:
            self.statIcon.set_blinking( False )
            self.statIcon.set_tooltip( 'gtt - Connection status unknown when not reporting' )
        
        return True # so that timer keeps calling us.

    def leftClickCb( self, widget, data=None ):
        self.shouldReport = not self.shouldReport
        if self.shouldReport:
            com.msg() # so we update the connection status before mouse is moved
            com.msg() # hmm.. don't know why it's needed twice
            self.statIcon.set_from_pixbuf( self.reportPixbuf )
        else:
            self.statIcon.set_from_pixbuf( self.noReportPixbuf )


    def rightClickCb( self, button, widget, data=None ):
        dialog = gtk.MessageDialog(
            parent         = None,
            flags          = gtk.DIALOG_DESTROY_WITH_PARENT,
            type           = gtk.MESSAGE_INFO,
            buttons        = gtk.BUTTONS_YES_NO,
            message_format = "Do you want to close the gtt client?")
        dialog.set_title('Close gtt?')
        dialog.set_default_size( 250, 100 )
        dialog.connect( 'response', self.destroyer )
        dialog.show()

    def destroyer( self, widget, response_id, data= None ):
        if response_id == gtk.RESPONSE_YES:
                widget.hide()
                gtk.main_quit()
        else:
                widget.hide()


class ErrorWindow:
    """Displays a window with an error message"""
    def __init__( self, message ):
        dialog = gtk.MessageDialog(
            parent         = None,
            flags          = gtk.DIALOG_DESTROY_WITH_PARENT,
            type           = gtk.MESSAGE_ERROR,
            buttons        = gtk.BUTTONS_CLOSE,
            #message_format = str(message)
            )
        dialog.set_title('gtt - Error')
        dialog.set_default_size( 250, 100 )
        dialog.set_resizable( True )
        dialog.set_markup( message )
        dialog.connect( 'response', self.destroyer )
        dialog.show()
        gtk.main()
        
    def destroyer( self, widget, response_id, data= None ):
        widget.hide()
        gtk.main_quit()
        


class param:
    """Used for storing parameter stuff.. not really necessary"""

def strToPortHost( string ):
    if ':' in string:
        host, port = string.split(':')[:2]
    else:
        host = None
        port = string
    try:
        port = int(port)
    except ValueError:
        print '%s is invalid not a valid port' % port
        ErrorWindow( '%s is invalid not a valid port' % port )
        sys.exit()
    if port < 0 or port > 0xFFFF:
        print '%u is invalid not a valid port' % port
        ErrorWindow( '%u is invalid not a valid port' % port )
        sys.exit()
    return ( port, host )
        

def printHelp():
    print \
"""
gtt     graphical/gui/gtk/whatever timing tool,
        as opposed to the older terminal timing tool (ttt).

        Copyright (C) 2009  Mårten Oscarsson
        This program comes with ABSOLUTELY NO WARRANTY.
        This is free software, and you are welcome to redistribute it
        under certain conditions.
        See the GNU General Public License for more details.


options:
        Options can be specified on the command line or in the file
        %s
        All times are specified in minutes.

-bt     Break time, after how long period of inactivity, during the same day,
        should a new timing period start. If work resumes after this time
        a new timing period will start and therefore the time of inactivity
        will not be treated as work. If for example you log in for 10min in
        the evening only these 10min are added to the work time. If you don't
        want this feature set the it to a very high value.

-la     Lunch after, after how much work time should it be considered that
        a lunch break has been made. When the working time is greater then
        this value time for lunch is automatically subtracted. (See below)

-ll     Lunch length, how much time should be subtracted for lunch.
        If you don't want this feature set the value to 0.

-dl     Day length, what is the expected working time each day. Saturday and
        Sunday is always set to 0.

-width  Minimum width of the window. Window will start with this width. 

-height Minimum height of the window. Window will start with this height. 

-nw     Number of weeks to display in the week view.

-iv     Initial view. Which view should be used at startup.
        If set to 'week' the week view will be used, else month view is used.

-ttd    Set the delimiter used for the total time column.
        Valid values are '.' (default) and ','.

-ttmd   Set the devider used for the minutes in the total time column.
        Valid values are '100' (default) and '60'.
        I.e. minutes are treated as 1/100 or 1/60 of an hour.

-server Start a server to which clients can connect and report work. The number of
        currently connected clients are shown to the left of the year chooser (if not 0).
        Valid values are 'port', '<host name>:<port>' or '<host ip address>:<port>'.
        If only port is specified the ip address of the host will automatically be
        discovered if possible.

-client Start in client mode. In this mode only a status icon will be displayed in the
        systray. Work will be reported to the server specified, probably on another
        computer.
        Left click on the icon to toggle reporting on/off. Right click on icon to quit.
        Blinking icon means no connection with server (blink will not occur when report
        is set to off - left click).
        Valid values are '<host name>:<port>' or '<host ip address>:<port>'.

-po     Print out the options that has been specified (mainly for debug).

-v      Print version.

-h      Print this text.


usage:

General gtt is a tool to record the time you work. It records the first and last
        time of the day that you move the mouse. It does not detect key strokes.
        
Lunch   By default gtt will automatically remove time for lunch. This can be
        turned off with '-ll 0' either as an argument when gtt is started or in
        the rc file.
        When automatic lunch is turned on it is still possible to turn lunch on
        or off for a certain day by clicking the check box. Once you have 
        manually set the check box to a value, that day is never going back to 
        automatic mode again.

Editing It is possible to change the recorded time by editing either the work
        times entry or the total time entry.
        If the total time is changed the work times will be changed to match the
        total time.  

Tot     The total time entry is displayed in hours only, e.g. 1.5 means one hour
        and 30 minutes. The reason for this is that it is the format used on my
        time report.

View    The View button toggles between month and week view.

Open    The Open button (only in month view) re-reads the displayed month from
        disk. It is only useful if you have edited it manually.

Save    The Save button (only in month view) writes the displayed month to disk.
        Current month (not the displayed month) is save automatically every 15
        minutes and when gtt is closed.

Files   The files with data for each month are stored in the directory\n        %s
"""%(rcFilePath, param.dataDir)

version = '1.2'

if __name__ == '__main__':
    # http://standards.freedesktop.org/basedir-spec/latest/ar01s03.html
    configHome = os.getenv( 'XDG_CONFIG_HOME' )
    dataHome   = os.getenv( 'XDG_DATA_HOME' )
    if not configHome:
        configHome = os.path.expanduser('~') + '/.config'
    if not dataHome:
        dataHome = os.path.expanduser('~') + '/.local/share'
    #get options
    rcFilePath = configHome + '/gtt/gtt.rc'
    param.dataDir = dataHome + '/gtt'
    opts = options( rcFilePath )
    opts.addOpt( {'-bt'     : [90],      # break time
                  '-la'     : [240],     # lunch after
                  '-ll'     : [40],      # lunch length
                  '-hl'     : [None],    # hide lunch
                  '-dl'     : [465],     # day length
                  '-width'  : [380],     # width
                  '-height' : [420],     # height
                  '-nw'     : [2],       # number of weeks
                  '-iv'     : ['month'], # initial view
                  '-ttd'    : ['.'],     # total time delimiter
                  '-ttmd'   : ['100'],   # total time minute devider
                  '-server' : [''],      # we should be a server, clients can connect
                  '-client' : [''],      # we should be a client, connect to a server
                  '-h'      : [None],    # help
                  '-v'      : [None],    # version
                  '-po'     : [None],    # print options
                  } )
    opts.updateValues()
    if opts.getOpt( '-po' ) != [None]:
        opts.printOpts()
    if opts.getOpt( '-h' ) != [None]:
        printHelp()
        sys.exit()
    if opts.getOpt( '-v' ) != [None]:
        print 'Version %s'%version
        sys.exit()
    param.idleBreakTime  = opts.getOpt( '-bt',     'int' )[0]
    param.lunchAfterMin  = opts.getOpt( '-la',     'int' )[0]
    param.lunchLength    = opts.getOpt( '-ll',     'int' )[0]
    param.workDayLength  = opts.getOpt( '-dl',     'int' )[0]
    param.width          = opts.getOpt( '-width',  'int' )[0]
    param.height         = opts.getOpt( '-height', 'int' )[0]
    param.numWeeksInView = opts.getOpt( '-nw',     'int' )[0]
    param.initialView    = 'month'
    if opts.getOpt( '-iv', 'str' )[0].lower() == 'week':
        param.initialView = 'week'
    param.showLunch = True
    if (opts.getOpt( '-hl' )[0] != None) or (param.lunchLength == 0):
        param.showLunch = False
    param.totTimeDelimiter = '.'
    if opts.getOpt( '-ttd', 'str' )[0] == ',':
        param.totTimeDelimiter = ','
    param.totTimeMinuteDevider = 100
    if opts.getOpt( '-ttmd', 'int' )[0] == 60:
        param.totTimeMinuteDevider = 60

    if opts.getOpt( '-server', 'str' )[0] != '' and opts.getOpt( '-client', 'str' )[0] != '':
        print "Can't be both client and server"
        ErrorWindow( "Can't be both client and server" )
        sys.exit()
        
    param.isServer = False
    if opts.getOpt( '-server', 'str' )[0] != '':
        string = opts.getOpt( '-server', 'str' )[0]
        port, host = strToPortHost( string )
        com = Communicator( 'server', port, host )
        param.isServer = True

    param.isClient = False
    if opts.getOpt( '-client', 'str' )[0] != '':
        string = opts.getOpt( '-client', 'str' )[0]
        port, host = strToPortHost( string )
        com = Communicator( 'client', port, host )
        param.isClient = True

    mp  = MousePointer()
    db  = DataBase()
    if param.isClient:
        ClientStatusIcon()
    else:
        MainWindow()
    gtk.main() # we will be stuck here untill we exit gtk

    if param.isClient or param.isServer:
        com.close()


##### TODO #####
# 
# 


##### CHANGELOG #####
# 
# 
# 1.2
# * Added options: -server and -client
#   to make it possible to report work from other computers


# 1.1.2
# * Just some minor edits in the help text.

# 1.1.1
# * Fixed bug in the hadling of weeks in begining/end of year. (week 0/53)

# 1.1
# * Added options: -po, -ttd and -ttmd.
# * Fixed so that total time is not updated when text is not changed. To prevent round off errors.
# * Removed leading '0' from hours in total time column.
# * Made it possible to omit minutes when editing times.

